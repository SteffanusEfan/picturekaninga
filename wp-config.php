<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'wp_kaninga1');
define('DB_NAME', 'devkaningapictures');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'd1g1beat');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JA=&Jr:pGJU1dUa/j:J];S_APx8T7l#I:xUX[pliA7yMe!PO+,1c}b4uz[X8OaML');
define('SECURE_AUTH_KEY',  ')FP2g,1/k5a uOkv$k5l+>U&PLy79iUF2(ju9<`)k,rhK9;sK6?Y@M(m26kf}F%%');
define('LOGGED_IN_KEY',    'j->6X7mNLwe2>o#:b.85wKyp^t`13TUp;qTk#&w%<X:Fut~?fKFLLL+f7<c >C+q');
define('NONCE_KEY',        'yF[c=%Fv)Q25!*;X0Blb&cN1*_]hEBC$kSp&|s>cMfw$f,F<m*Uz/CRi}q}w32bn');
define('AUTH_SALT',        'a=E8@_[|Ty?0(KK.Fk){{c{SXPl!j+HFq#j8D~+Cyus=*f`*^yb)XXfEd)-[dNrO');
define('SECURE_AUTH_SALT', '_qs%?2t;C%Fi:c/]g3wC.4mKv;<e:&K%`-cu4/X)~r)NTGxeU@=l0IsvkTS [3u-');
define('LOGGED_IN_SALT',   'dx4R!H/L_i<r7-=>ZYHID2k7Z0y`r~@k=g/2&Mr(><UFZN4l>.l?/n&tJPbgsIl3');
define('NONCE_SALT',       'x_q.]hD~a5Q6`=^aSs~:vVH+VN9bLX4JMA$mX<{9JRS:8y^17K`-Bz.=CYs{:3^8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
