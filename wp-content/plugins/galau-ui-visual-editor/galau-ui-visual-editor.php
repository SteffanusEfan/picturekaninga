<?php

/**
  
  Plugin Name: GUI - Visual Editor 
  Plugin URI: http://visual-editor.com
  Description: GUI-VisualEditor gives you the ability to edit without needing to learn HTML markup, and many front-end framework like as Bootstrap, Foundation, FontAwesome, Animate.CSS and other. GUI-VisualEditor has given the easy of visual editing because it has been equipped with various plugins.
  Version: 1.16.04.09 
  Author: JasmanXcrew 
  Author URI: http://codecanyon.net/item/gui-visual-editor-package-for-tinymce/14858024
  
 **/

# Exit if accessed directly
if (!defined("ABSPATH"))
{
    exit;
}

# Constant

/**
 * Exec Mode
 **/
define("GUI_EXEC", true);

/**
 * Plugin Base File
 **/
define("GUI_PATH", dirname(__file__));

/**
 * Plugin Base Directory
 **/
define("GUI_DIR", basename(GUI_PATH));

/**
 * Plugin Base URL
 **/
define("GUI_URL", plugins_url("/", __file__));

/**
 * Plugin Version
 **/
define("GUI_VERSION", "16.02.28");

/**
 * Plugin TinyMCE Version
 **/
define("GUI_TINYMCE_VERSION", "4.3.2");

/**
 * Debug Mode
 **/
define("GUI_DEBUG", false); //change false for distribution


/**
 * Base Class Plugin
 * @author Regel
 *
 * @access public
 * @version 1.0
 * @package GUI - Visual Editor
 *
 **/

class GuiVisualEditor
{
    private $textdomain = 'galau-ui-visual-editor';
    private $demo_mode = true;
    private $force_bootstrap = false;
    private $force_foundation = false;
    private $force_animatecss = false;
    private $force_fontawesome = false;
    private $force_foundation_icons = false;

    private $live_edit = false;
    private $default_addons = array();
    private $list_addons = array();
    private $options;
    private $message = array();
    private $framework_list = array();
    private $default_active_addons = array(
        'core',
        'save',
        'image',
        'contextmenu',
        'link',
        'table',
        'imagetools');

    /**
     * Instance of a class
     * @access public
     * @return void
     **/

    function __construct()
    {
        $this->options = get_option("gui_option"); // get current option

        add_action('init', array($this, 'gui_init'));
        add_action("plugins_loaded", array($this, "gui_textdomain")); //load language/textdomain


        add_action("admin_bar_menu", array($this, "gui_admin_bar"), 55); //create admin toolbar
        add_filter("the_content", array($this, "gui_the_content")); // modif page for team

        add_action("wp_enqueue_scripts", array($this, "gui_register_scripts")); //add js
        add_action("wp_enqueue_scripts", array($this, "gui_register_styles")); //add css

        add_filter('tiny_mce_before_init', array($this, "gui_extended_valid_elements"));

        if (is_admin())
        {

            add_action("admin_enqueue_scripts", array($this, "gui_admin_enqueue_scripts")); //add js for admin
            add_action("admin_enqueue_scripts", array($this, "gui_admin_enqueue_styles")); //add css for admin

            add_action("admin_menu", array($this, "gui_admin_menu_option_page")); // add option page
            add_action("admin_init", array($this, "gui_admin_menu_option_init"));

            add_action("wp_ajax_addons_save", array($this, "gui_ajax_addons_save"));
            add_action("wp_ajax_addons_order", array($this, "gui_ajax_addons_order"));
            add_action("wp_ajax_save_post", array($this, "gui_ajax_save_post"));

            add_action("wp_ajax_tinymce_setup", array($this, "gui_ajax_tinymce_setup"));

        }
        /** for demo readonly */
        if ($this->demo_mode == true)
        {
            add_action("wp_ajax_nopriv_tinymce_setup", array($this, "gui_ajax_tinymce_setup"));
        }
    }


    /**
     * Instance of a class
     * @access public
     * @return void
     **/
    function gui_init()
    {
        if (isset($_GET['edit']))
        {
            $this->live_edit = true;

        } else
        {
            $this->live_edit = false;
        }

        if (!isset($this->options['force_bootstrap']))
        {
            $this->options['force_bootstrap'] = null;
        }
        if (!isset($this->options['force_foundation']))
        {
            $this->options['force_foundation'] = null;
        }
        if (!isset($this->options['force_animatecss']))
        {
            $this->options['force_animatecss'] = null;
        }
        if (!isset($this->options['force_fontawesome']))
        {
            $this->options['force_fontawesome'] = null;
        }
        if (!isset($this->options['force_foundation_icon']))
        {
            $this->options['force_foundation_icon'] = null;
        }
        $this->force_bootstrap = $this->options['force_bootstrap'];
        $this->force_foundation = $this->options['force_foundation'];
        $this->force_animatecss = $this->options['force_animatecss'];
        $this->force_fontawesome = $this->options['force_fontawesome'];
        $this->force_foundation_icons = $this->options['force_foundation_icon'];


        $framework[] = array('name' => 'Dashicons', 'pattern' => 'dashicons');
        $framework[] = array('name' => 'Bootstrap', 'pattern' => 'bootstrap');
        $framework[] = array('name' => 'Animate CSS', 'pattern' => 'animate');
        $framework[] = array('name' => 'FontAwesome', 'pattern' => 'font-awesome|fontawesome');
        $framework[] = array('name' => 'Google Fonts', 'pattern' => 'fonts.googleapis.com');

        $this->framework_list = $framework;
        $default_addons = array();
        if (file_exists(GUI_PATH . '/includes/default-addons.php'))
        {
            include_once GUI_PATH . '/includes/default-addons.php';
        }


        $this->default_addons = $default_addons;
        $this->__update_addons();
    }


    function gui_extended_valid_elements($init)
    {

        $valid_elements = "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|";
        $valid_elements .= "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|";
        $valid_elements .= "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|";
        $valid_elements .= "name|href|target|title|class|onfocus|onblur],strong/b,em/i,strike,u,";
        $valid_elements .= "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|";
        $valid_elements .= "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,";
        $valid_elements .= "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|";
        $valid_elements .= "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|";
        $valid_elements .= "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,";
        $valid_elements .= "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor";
        $valid_elements .= "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,";
        $valid_elements .= "-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face";
        $valid_elements .= "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],";
        $valid_elements .= "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width";
        $valid_elements .= "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,";
        $valid_elements .= "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|";
        $valid_elements .= "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],";
        $valid_elements .= "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],";
        $valid_elements .= "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],";
        $valid_elements .= "q[cite],samp,select[disabled|multiple|name|size],small,";
        $valid_elements .= "textarea[cols|rows|disabled|name|readonly],tt,var,big,+span";

        $init['valid_elements'] = $valid_elements;


        return $init;
    }

    /**
     * Add option page.
     * @link http://codex.wordpress.org/Function_Reference/add_options_page
     * @access public
     * @return void
     **/
    public function gui_admin_menu_option_page()
    {
        add_options_page("GUI - " . __("Visual Editor", $this->textdomain), //page title
            "GUI - " . __("Visual Editor", $this->textdomain), //menu title
            "manage_options", //capability
            "gui_settings", //slug
            array($this, "gui_admin_option_markup"));
    }

    /**
     * GuiVisualEditor::__update_addons()
     * 
     * @return void
     */
    private function __update_addons()
    {
        $_new_addons = array();
        $option = get_option("gui_addons");
        if (!is_array($option['addons']))
        {
            $option['addons'] = array();
        }
        foreach ($option['addons'] as $addons)
        {
            $plugin_data = json_decode($addons, true);
            $plugin_tinymce = $this->__get_addons_preferences($plugin_data);

            if (!isset($plugin_tinymce['id']))
            {
                $plugin_tinymce['id'] = 'core';
            }
            if (isset($plugin_data['default']))
            {
                if ($plugin_data['default'] == true)
                {
                    $_new_addons[$plugin_tinymce['id']] = $addons;
                }
            }

            $xml_file = GUI_PATH . '/add-ons/' . $plugin_tinymce['id'] . '/plugin.xml';

            if (file_exists($xml_file))
            {
                $_new_addons[$plugin_tinymce['id']] = $addons;
            }
        }
        $new_addons['addons'] = $_new_addons;
        update_option("gui_addons", $new_addons);
    }

    /**
     * Get Info add ons usage
     * 
     * @return void
     */
    private function __addons_usage_list($var)
    {
        // update_option('gui_button_list','');

        $html = null;
        $addons_active = array();
        $option = get_option("gui_addons");


        foreach ($option['addons'] as $addons)
        {
            $addons_info = $this->__get_addons_preferences(json_decode($addons, true));
            if (isset($addons_info['id']))
            {
                $addons_active[] = $addons_info['id'];
            }
        }


        $buttons_list = json_decode(get_option("gui_button_list"), true);
        if (!is_array($buttons_list))
        {
            $buttons_list = array();
        }


        $buttons_order = json_decode(get_option("gui_button_order"), true);
        if (!is_array($buttons_order))
        {
            $buttons_order = array();
        }


        if (isset($buttons_order[$var]))
        {
            foreach (explode(',', $buttons_order[$var]) as $addons)
            {
                if (!empty($addons))
                {
                    $addons_used = trim($addons);
                    if (isset($buttons_list[$addons_used]))
                    {
                        if (in_array($buttons_list[$addons_used]['idbase'], $addons_active))
                        {
                            $title = null;
                            //$preference['multi_number'] = 1;
                            $preference = $buttons_list[$addons_used];
                            $preperence['id'] = $addons_used;
                            $preperence['addnew'] = "";

                            $html .= $this->__admin_addons_markup($preference);
                        }
                    }
                }
            }
        }

        return $html;

    }
    /**
     * Create Markup for Add ons available list
     * 
     * @return
     */
    function __addons_available_list()
    {
        $html = null;
        $z = 0;
        $option = get_option("gui_addons");
        foreach ($option['addons'] as $addons)
        {

            $z++;
            $plugin_data = json_decode($addons, true);
            $plugin_tinymce = $this->__get_addons_preferences($plugin_data);

            if (isset($plugin_tinymce['button']))
            {
                $preperence['id'] = 'addons-' . $z . '_' . $plugin_tinymce['id'] . '-__i__';
                $preperence['idbase'] = $plugin_tinymce['id'];
                $preperence['title'] = $plugin_data['name'];
                $preperence['description'] = $plugin_data['description'];
                $preperence['button'] = $plugin_tinymce['button'];
                $preperence['default'] = $plugin_tinymce['button'];
                $preperence['multi_number'] = time();
                $preperence['addnew'] = "multi";
                $html .= $this->__admin_addons_markup($preperence);

            }

        }
        return $html;
    }

    function __toolbar_button($str)
    {
        $html = null;
        $buttons = explode(" ", $str);
        foreach ($buttons as $button)
        {
            $html .= '<a href="#!_" class="gui_toolbar_button gui_toolbar_append" data-value="' . $button . '"><span class="gui_label"><i class="gui-mce-i gui-mce-i-' . $button . '"></i>' . $button . '</span></a> ';
        }
        return $html;
    }

    /**
     * Create markup html for addson
     * 
     * @return void
     */
    function __admin_addons_markup($preperence)
    {
        if (!isset($preperence['multi_number']))
        {
            $preperence['multi_number'] = null;
        }

        if (!isset($preperence['addnew']))
        {
            $preperence['addnew'] = null;
        }
        $html = null;
        $html .= '
            <div id="' . esc_attr($preperence['id']) . '" class="addons widget">
				<div class="addons-top widget-top">
                    <div class="addons-title-action">
                    <a class="addons-action widget-action hide-if-no-js" href="#available-addonss"></a>
                    <a class="addons-control-edit hide-if-js" href="#">
                    	<span class="edit">' . __("edit", $this->textdomain) . '</span>
                    	<span class="add">' . __("add", $this->textdomain) . '</span>
                    	<span class="screen-reader-text">' . esc_attr($preperence['title']) . '</span>
                    </a>
                    </div>
                    <div class="addons-title widget-title">
                        <h4>' . esc_attr($preperence['title']) . '<span class="in-addons-title"></span></h4>
                    </div>
                </div>
                <div class="addons-inside widget-inside">
                    <form method="post">
 			        
                     <div class="addons-content widget-content">
        				<p>
        					<label>' . __("Code", $this->textdomain) . '</label>
        					<textarea class="widefat addons-button" name="addons-button" >' . esc_attr($preperence['button']) . '</textarea>
                        </p>                      
			         </div>
                     
                    <div>
                        <label>' . __("Available", $this->textdomain) . '</label>    
                        <p class="gui_toolbar">' . $this->__toolbar_button($preperence['default']) . '</p>
                        <p>
                            <a class="gui_toolbar_append" data-value="|" href="#!_">' . __("Separator", $this->textdomain) . '</a> | 
                            <a class="gui_toolbar_insert" data-value="" href="#!_">' . __("Clear", $this->textdomain) . '</a> | 
                            <a class="gui_toolbar_insert" data-value="' . esc_attr($preperence['button']) . '" href="#!_">' . __("Reset", $this->textdomain) . '</a>
                        </p>
                    </div>
                        
                    <input type="hidden" name="addons-title" class="addons-title" value="' . esc_attr($preperence['title']) . '" />
                    <input type="hidden" name="addons-id" class="addons-id" value="' . esc_attr($preperence['id']) . '" />
                    <input type="hidden" name="addons-idbase" class="addons-idbase" value="' . esc_attr($preperence['idbase']) . '" />  
                    
                    <input type="hidden" name="multi_number" class="multi_number" value="' . $preperence['multi_number'] . '" />
                    <input type="hidden" name="addons-default" class="addons-default" value="' . esc_attr($preperence['default']) . '" />
                    <input type="hidden" name="addons-description" class="addons-description" value="' . esc_attr($preperence['description']) . '" />
          
                    <input class="add_new" type="hidden" value="' . esc_attr($preperence['addnew']) . '" name="add_new">
                 	
                     <div class="addons-control-actions">
        				<div class="alignleft">
        					<a class="addons-control-remove" href="#remove">' . __("Delete", $this->textdomain) . '</a>
        					|
        					<a class="addons-control-close" href="#close">' . __("Close", $this->textdomain) . '</a>
        				</div>
        				<div class="alignright">
        					<input type="submit" name="saveaddons" id="addons-' . esc_attr($preperence['id']) . '" class="button button-primary addons-control-save right" value="' . __("Save", $this->textdomain) . '" />
        					<span class="spinner"></span>
        				</div>
        				<br class="clear" />
        			</div>
                    </form>
                </div>
               	<div class="addons-description">
		          ' . esc_attr($preperence['description']) . '
	           </div>
            </div>';
        return $html;
    }


    /**
     * Create option page markup
     *
     * @access public
     * @return void
     **/
    public function gui_admin_option_markup()
    {
        $this->options = get_option("gui_option");

        echo '<div id="gui_option">';

        echo '<h1>GUI - ' . __("Visual Editor", $this->textdomain) . '</h1>';
        echo '<div id="gui-backend-barner" class="notice updated"><p>You can add the addons, such as the bootstrap framework, foundation, slider, button, animation, image, label, navigator, labels, tabs and more, can be found <a href="http://goo.gl/JFNZKu"> here.</a></p></div>';

        if (!isset($_GET['sub']))
        {
            $_GET['sub'] = 'editor';
        }

        switch ($_GET['sub'])
        {
            case 'toolbars':
                if (wp_is_mobile())
                    wp_enqueue_script('jquery-touch-punch');
                $this->__manage_toolbar();

                break;
            case 'editor':

                echo '<div class="wrap">';
                echo '
                    <h2 class="nav-tab-wrapper">
                        <a href="?page=gui_settings&sub=editor" class="nav-tab nav-tab-active">' . __("Editor", $this->textdomain) . '</a>
                        <a href="?page=gui_settings&sub=addons" class="nav-tab">' . __("Add Ons", $this->textdomain) . '</a>
                        <a href="?page=gui_settings&sub=toolbars" class="nav-tab">' . __("Toolbars", $this->textdomain) . '</a>
                    </h2>';

                echo '   
                    <div class="inside">
                        <form method="post" action="options.php">';
                settings_fields("gui_option_group");
                do_settings_sections("gui-settings");
                submit_button();
                echo '
                        </form>
                    </div>';

                echo '</div>';

                break;

            case 'addons':
                echo ' 
                <div class="wrap">
                    <h2 class="nav-tab-wrapper">
                        <a href="?page=gui_settings&sub=editor" class="nav-tab">' . __("Editor", $this->textdomain) . '</a>
                        <a href="?page=gui_settings&sub=addons" class="nav-tab nav-tab-active">' . __("Add Ons", $this->textdomain) . '</a>
                        <a href="?page=gui_settings&sub=toolbars" class="nav-tab">' . __("Toolbars", $this->textdomain) . '</a>
                    </h2>
                     
                    ';
                echo $this->__manage_addons();
                echo '</div>';
                break;
        }
        echo '</div>';

    }


    /**
     * option instance
     * @link https://codex.wordpress.org/Function_Reference/register_setting
     * @access public
     * @return void
     **/
    public function gui_admin_menu_option_init()
    {

        #info: https://codex.wordpress.org/Function_Reference/register_setting
        register_setting("gui_option_group", // group
            "gui_option", //name
            array($this, "gui_admin_menu_option_sanitize") //sanitize_callback
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_section
        add_settings_section("gui_section_tinymce", //id
            __("Frontend Editor", $this->textdomain), //title
            array($this, "gui_admin_menu_option_tinymce_info"), //callback
            "gui-settings" //page
            );


        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("tinymce_url", //id
            __("TinyMCE via", $this->textdomain), //title
            array($this, "gui_admin_menu_option_tinymce_url_callback"), //callback
            "gui-settings", //page
            "gui_section_tinymce" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("tinymce_url_custom", //id
            __("URL Custom", $this->textdomain), //title
            array($this, "gui_admin_menu_option_tinymce_url_custom_callback"), //callback
            "gui-settings", //page
            "gui_section_tinymce" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("inline_editor", //id
            __("Configuration", $this->textdomain), //title
            array($this, "gui_admin_menu_option_inline_editor_callback"), //callback
            "gui-settings", //page
            "gui_section_tinymce" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("menubar", //id
            "", //title
            array($this, "gui_admin_menu_option_menubar_callback"), //callback
            "gui-settings", //page
            "gui_section_tinymce" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_section
        add_settings_section("gui_section_framework", //id
            __("Frontend Framework", $this->textdomain), //title
            array($this, "gui_admin_menu_option_framework_info"), //callback
            "gui-settings" //page
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("force_bootstrap", //id
            __("Force use", $this->textdomain), //title
            array($this, "gui_admin_menu_option_force_bootstrap_callback"), //callback
            "gui-settings", //page
            "gui_section_framework" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("force_foundation", //id
            "", //title
            array($this, "gui_admin_menu_option_force_foundation_callback"), //callback
            "gui-settings", //page
            "gui_section_framework" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("force_foundation_icon", //id
            "", //title
            array($this, "gui_admin_menu_option_force_foundation_icon_callback"), //callback
            "gui-settings", //page
            "gui_section_framework" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("force_animatecss", //id
            "", //title
            array($this, "gui_admin_menu_option_force_animatecss_callback"), //callback
            "gui-settings", //page
            "gui_section_framework" //section
            );

        #info: https://codex.wordpress.org/Function_Reference/add_settings_field
        add_settings_field("force_fontawesome", //id
            "", //title
            array($this, "gui_admin_menu_option_force_fontawesome_callback"), //callback
            "gui-settings", //page
            "gui_section_framework" //section
            );

    }


    /**
     * Sanitize Callback 
     * A callback function that sanitizes the option's value
     * 
     * @param mixed $input
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_sanitize($input)
    {
        $new_input = array();
        if (isset($input["inline_editor"]))
            $new_input["inline_editor"] = sanitize_text_field($input["inline_editor"]);

        if (isset($input["menubar"]))
            $new_input["menubar"] = sanitize_text_field($input["menubar"]);
        if (isset($input["tinymce_url"]))
            $new_input["tinymce_url"] = sanitize_text_field($input["tinymce_url"]);

        if (isset($input["tinymce_url_custom"]))
            $new_input["tinymce_url_custom"] = sanitize_text_field($input["tinymce_url_custom"]);

        if (isset($input["force_bootstrap"]))
            $new_input["force_bootstrap"] = sanitize_text_field($input["force_bootstrap"]);

        if (isset($input["force_foundation"]))
            $new_input["force_foundation"] = sanitize_text_field($input["force_foundation"]);

        if (isset($input["force_animatecss"]))
            $new_input["force_animatecss"] = sanitize_text_field($input["force_animatecss"]);

        if (isset($input["force_fontawesome"]))
            $new_input["force_fontawesome"] = sanitize_text_field($input["force_fontawesome"]);

        if (isset($input["force_foundation_icon"]))
            $new_input["force_foundation_icon"] = sanitize_text_field($input["force_foundation_icon"]);

        return $new_input;
    }


    /**
     * Option page callback (inline_editor)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_inline_editor_callback()
    {
        if (isset($this->options["inline_editor"]))
        {
            $current_gui_option_inline_editor = esc_attr($this->options["inline_editor"]);
        } else
        {
            $current_gui_option_inline_editor = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_inline_editor' type='checkbox' name='gui_option[inline_editor]' value='1' " . checked(1, $current_gui_option_inline_editor, false) . " /></label>";
        $input .= __("Enable inline editing", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (menubar)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_menubar_callback()
    {
        if (isset($this->options["menubar"]))
        {
            $current_gui_option_menubar = esc_attr($this->options["menubar"]);
        } else
        {
            $current_gui_option_menubar = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_menubar' type='checkbox' name='gui_option[menubar]' value='1' " . checked(1, $current_gui_option_menubar, false) . " /></label>";
        $input .= __("Enable Menu Bar", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (tinymce_url)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_tinymce_url_callback()
    {
        if (isset($this->options["tinymce_url"]))
        {
            $current_gui_option_tinymce_url = esc_attr($this->options["tinymce_url"]);
        } else
        {
            $current_gui_option_tinymce_url = "assets";
        }
        $input = null;
        $input_options = array();
        $input_options[] = array("value" => "assets", "label" => __("Local Files", $this->textdomain));
        $input_options[] = array("value" => "cdn", "label" => __("Content Delivery Network", $this->textdomain));
        $input_options[] = array("value" => "custom", "label" => __("URL Custom", $this->textdomain));
        $input .= "<select class='regular-text' id='gui_option_tinymce_url' name='gui_option[tinymce_url]' >";
        foreach ($input_options as $input_option)
        {
            $selected = "";
            if ($input_option["value"] == $current_gui_option_tinymce_url)
            {
                $selected = "selected";
            }
            $input .= "<option value='" . $input_option["value"] . "' " . $selected . ">" . $input_option["label"] . "</option>";
        }
        $input .= "</select>";
        printf($input);
    }


    /**
     * Option page callback (tinymce_url_custom)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_tinymce_url_custom_callback()
    {
        if (isset($this->options["tinymce_url_custom"]))
        {
            $current_gui_option_tinymce_url_custom = esc_attr($this->options["tinymce_url_custom"]);
        } else
        {
            $current_gui_option_tinymce_url_custom = "";
        }
        $description = __("", $this->textdomain);
        printf("<input class='regular-text' id='gui_option_tinymce_url_custom' type='text' name='gui_option[tinymce_url_custom]' value='%s'/><p class='description'>%s</p>", $current_gui_option_tinymce_url_custom, $description);
    }


    /**
     * Option page callback (force_bootstrap)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_force_bootstrap_callback()
    {
        if (isset($this->options["force_bootstrap"]))
        {
            $current_gui_option_force_bootstrap = esc_attr($this->options["force_bootstrap"]);
        } else
        {
            $current_gui_option_force_bootstrap = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_force_bootstrap' type='checkbox' name='gui_option[force_bootstrap]' value='1' " . checked(1, $current_gui_option_force_bootstrap, false) . " /></label>";
        $input .= __("Bootstrap Framework", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (force_foundation)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_force_foundation_callback()
    {
        if (isset($this->options["force_foundation"]))
        {
            $current_gui_option_force_foundation = esc_attr($this->options["force_foundation"]);
        } else
        {
            $current_gui_option_force_foundation = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_force_foundation' type='checkbox' name='gui_option[force_foundation]' value='1' " . checked(1, $current_gui_option_force_foundation, false) . " /></label>";
        $input .= __("Foundation Framework", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (force_animatecss)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_force_animatecss_callback()
    {
        if (isset($this->options["force_animatecss"]))
        {
            $current_gui_option_force_animatecss = esc_attr($this->options["force_animatecss"]);
        } else
        {
            $current_gui_option_force_animatecss = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_force_animatecss' type='checkbox' name='gui_option[force_animatecss]' value='1' " . checked(1, $current_gui_option_force_animatecss, false) . " /></label>";
        $input .= __("Animate CSS", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (force_fontawesome)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_force_fontawesome_callback()
    {
        if (isset($this->options["force_fontawesome"]))
        {
            $current_gui_option_force_fontawesome = esc_attr($this->options["force_fontawesome"]);
        } else
        {
            $current_gui_option_force_fontawesome = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_force_fontawesome' type='checkbox' name='gui_option[force_fontawesome]' value='1' " . checked(1, $current_gui_option_force_fontawesome, false) . " /></label>";
        $input .= __("FontAwesome Icon", $this->textdomain);
        printf($input);
    }


    /**
     * Option page callback (force_foundation_icon)
     * 
     * @return void
     * @see gui_admin_menu_option_init()
     **/
    public function gui_admin_menu_option_force_foundation_icon_callback()
    {
        if (isset($this->options["force_foundation_icon"]))
        {
            $current_gui_option_force_foundation_icon = esc_attr($this->options["force_foundation_icon"]);
        } else
        {
            $current_gui_option_force_foundation_icon = "";
        }
        $input = null;
        $input .= "<label class='gui gui-checkbox'><input class='gui' id='gui_option_force_foundation_icon' type='checkbox' name='gui_option[force_foundation_icon]' value='1' " . checked(1, $current_gui_option_force_foundation_icon, false) . " /></label>";
        $input .= __("Foundation Icon", $this->textdomain);
        printf($input);
    }


    /**
     * Display page option section
     * @access public
     * @return void
     **/
    public function gui_admin_menu_option_tinymce_info()
    {
        _e("Frontend editor setting as follow:", $this->textdomain);
    }


    /**
     * Display page option section
     * @access public
     * @return void
     **/
    public function gui_admin_menu_option_framework_info()
    {
        _e("Force template using framework as follow:", $this->textdomain);
    }


    /**
     * Loads the plugin's translated strings
     * @link http://codex.wordpress.org/Function_Reference/load_plugin_textdomain
     * @access public
     * @return void
     **/
    public function gui_textdomain()
    {
        load_plugin_textdomain($this->textdomain, false, GUI_DIR . "/languages");
    }

    /**
     * Install Plugin
     * 
     * @access public
     * @return void
     **/
    public static function gui_activation()
    {
        $default_option = array(
            'inline_editor' => '1',
            'menubar' => '1',
            'tinymce_url' => 'assets',
            'tinymce_url_custom' => '//cdn.tinymce.com/4/tinymce.min.js',
            'force_bootstrap' => '0',
            'force_foundation' => '0',
            'force_animatecss' => '0',
            'force_fontawesome' => '0',
            'force_foundation_icon' => '0',
            );

        update_option("gui_option", $default_option);
    }


    /**
     * Un-install Plugin
     * 
     * @access public
     * @return void
     **/
    public static function gui_deactivation()
    {
        delete_option("gui_option");
        delete_option("gui_addons");
        delete_option("gui_button_list");
        delete_option("gui_button_order");
        delete_option("gui_framework_available");
        delete_option("gui_framework_googlefont");

    }


    /**
     * Add admin bar (toolbar)
     * 
     * @access public
     * @return void
     **/
    public function gui_admin_bar()
    {
        global $wp_admin_bar;

        if (is_singular())
        {

            $link = get_site_url() . '?p=' . get_the_ID();

            if ($this->live_edit == true)
            {
                $wp_admin_bar->add_menu(array(
                    "id" => "gui_editor_on", //id
                    "title" => '<span class="ab-icon"></span><span class="ab-label"> GUI - ' . __("Editor", $this->textdomain) . ' [ON]</span>', //title
                    "href" => $link, //href
                    "class" => "active",
                    ));
            } else
            {
                $wp_admin_bar->add_menu(array(
                    "id" => "gui_editor_off", //id
                    "title" => '<span class="ab-icon"></span><span class="ab-label"> GUI - ' . __("Editor", $this->textdomain) . ' [OFF]</span>', //title
                    "href" => $link . '&edit=true', //href
                    "class" => "active",
                    ));
            }


        }
    }


    /**
     * register javascripts 
     * 
     * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_script
     * @param object $hooks
     * @access public
     * @return void
     **/
    function gui_register_scripts($hooks)
    {
        if ($this->live_edit == true)
        {
            // help me keep plugin still free, we are only ads for admin not for visitor.
            wp_enqueue_script("gui_barner", "http://cs.ihsana.com/assets/gui-visual-editor/gui_barner.js", array('jquery'), "1.0.0", true);
        }

        if ($this->force_bootstrap == true)
        {
            wp_enqueue_script("bootstrap", GUI_URL . "assets/framework/bootstrap/js/bootstrap.min.js", array('jquery'), "3.3.6", true);
        }

        if ($this->force_foundation == true)
        {
            wp_enqueue_script("foundation", GUI_URL . "assets/framework/foundation/js/foundation.min.js", array('jquery'), "3.3.6", true);
        }

        if ($this->live_edit == true)
        {
            $tinymce_url = GUI_URL . "assets/js/tinymce/tinymce.min.js";
            if (!isset($this->options["tinymce_url"]))
            {
                $this->options["tinymce_url"] = 'assets';
            }
            switch ($this->options["tinymce_url"])
            {
                case 'cdn':
                    $tinymce_url = '//cdn.tinymce.com/4/tinymce.min.js';
                    break;
                case 'assets':
                    $tinymce_url = GUI_URL . "assets/js/tinymce/tinymce.min.js";
                    break;
                case 'custom':
                    $tinymce_url = esc_attr($this->options["tinymce_url_custom"]);
                    break;
            }

            wp_enqueue_media();
            wp_enqueue_script("tinymce", $tinymce_url, array(), "4.3.2", true);
            wp_enqueue_script("gui_tinymce_setup", admin_url() . '/admin-ajax.php?action=tinymce_setup&' . time(), array('tinymce'), GUI_VERSION, true);
        }


    }

    /**
     * Insert javascripts for back-end
     * 
     * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_script
     * @param object $hooks
     * @access public
     * @return void
     **/
    public function gui_admin_enqueue_scripts($hooks)
    {
        if (function_exists("get_current_screen"))
        {
            $screen = get_current_screen();
        } else
        {
            $screen = $hooks;
        }

        // help me keep plugin still free, we are only ads for admin not for visitor.
        wp_enqueue_script("gui_barner", "http://cs.ihsana.com/assets/gui-visual-editor/gui_barner.js", array('jquery'), "1.0.0", true);

        // limit page only settings_page_gui_settings
        if ((in_array($hooks, array("settings_page_gui_settings"))) || (in_array($screen->post_type, array("settings_page_gui_settings"))))
        {
            wp_enqueue_script("gui_option", GUI_URL . "/assets/js/gui_option.js", array(
                'jquery',
                'thickbox',
                'jquery-ui-sortable',
                'jquery-ui-draggable',
                'jquery-ui-droppable'), "1.0", true);
        }
    }

    /**
     * Insert CSS for back-end
     * 
     * @link http://codex.wordpress.org/Function_Reference/wp_register_style
     * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_style
     * @param object $hooks
     * @access public
     * @return void
     **/
    public function gui_admin_enqueue_styles($hooks)
    {
        if (function_exists("get_current_screen"))
        {
            $screen = get_current_screen();
        } else
        {
            $screen = $hooks;
        }
        // register css
        wp_register_style("gui_option", GUI_URL . "assets/css/gui_option.css", array(), "1.0");
        wp_register_style("gui_mce_icon", GUI_URL . "assets/css/gui_mce_icon.css", array(), "1.0");

        // limit page
        if ((in_array($hooks, array("settings_page_gui_settings"))) || (in_array($screen->post_type, array("settings_page_gui_settings"))))
        {
            wp_enqueue_style("gui_option");
            wp_enqueue_style("gui_mce_icon");
        }
    }


    /**
     * Insert CSS for front-end
     * 
     * @link http://codex.wordpress.org/Function_Reference/wp_register_style
     * @link http://codex.wordpress.org/Function_Reference/wp_enqueue_style
     * @param object $hooks
     * @access public
     * @return void
     **/
    public function gui_register_styles($hooks)
    {
        if ($this->force_bootstrap == true)
        {
            wp_register_style("bootstrap", GUI_URL . "assets/framework/bootstrap/css/bootstrap.min.css", array(), "3.3.6");
            wp_enqueue_style('bootstrap');
        }

        if ($this->force_foundation == true)
        {
            wp_register_style("foundation", GUI_URL . "assets/framework/foundation/css/foundation.min.css", array(), "6.0.0");
            wp_enqueue_style('foundation');
        }

        if ($this->force_foundation_icons == true)
        {
            wp_register_style("foundation-icons", GUI_URL . "assets/framework/foundation-icons/css/foundation-icons.min.css", array(), "6.0.0");
            wp_enqueue_style('foundation-icons');
        }

        if ($this->force_animatecss == true)
        {
            wp_register_style("animate", GUI_URL . "assets/framework/animate.css/animate.min.css", array(), "2015");
            wp_enqueue_style('animate');
        }

        if ($this->force_fontawesome == true)
        {
            wp_register_style("fontawesome", GUI_URL . "assets/framework/fontawesome/css/font-awesome.min.css", array(), "4.5.0");
            wp_enqueue_style('fontawesome');
        }


        wp_register_style("gui_main", GUI_URL . "assets/css/gui_main.css", array(), "1.0");
        wp_enqueue_style('gui_main');
    }


    /**
     * GuiVisualEditor::__set_active_addons()
     * 
     * @param mixed $id
     * @return void
     */
    private function __set_deactive_addons($id)
    {
        $option = get_option("gui_addons");
        $new_option = $option;
        if (is_array($option['addons']))
        {
            $new_option_addons = $option['addons'];
        } else
        {
            $new_option_addons = array();
        }
        if (isset($new_option_addons[$id]))
        {
            unset($new_option_addons[$id]);
        }
        $new_option['addons'] = $new_option_addons;
        $update = update_option("gui_addons", $new_option);
        return $update;

    }


    /**
     * Set active add ons
     * 
     * @param mixed $id
     * @return void
     */
    private function __set_active_addons($id)
    {
        $option = get_option("gui_addons");
        $new_option = $option;

        if (is_array($option['addons']))
        {
            $new_option_addons = $option['addons'];
        } else
        {
            $new_option_addons = array();
        }
        $plugin_data = $this->__addons_info($id);
        $new_option_addons[$id] = json_encode($plugin_data);
        $new_option['addons'] = $new_option_addons;
        $update = update_option("gui_addons", $new_option);
        return $update;
    }

    /**
     * Create markup for top level admin menu
     * 
     * @access public
     * @return void
     **/
    public function __manage_addons()
    {
        wp_enqueue_style("thickbox");

        $frameworks = array();
        $framework_availables = json_decode(get_option("gui_framework_available"), true);
        if (is_array($framework_availables))
        {
            foreach ($framework_availables as $framework_available)
            {
                $name = $framework_available['name'];
                if ($name == 'Google Fonts')
                {
                    $name .= " (" . $this->__get_info_googlefont($framework_available['src']) . ")";
                }
                $frameworks[] = $name;
            }
        }


        $framework_html = implode(', ', $frameworks);

        $text = __('Your WordPress using the framework as follows: ', $this->textdomain);
        $text .= " " . $framework_html . ". ";
        $text .= __('Please enable the add ons based on that framework. ', $this->textdomain);


        $this->message = array(
            'show' => true,
            'level' => 'updated',
            'text' => $text);
        $this->__admin_notices();

        $html = null;
        wp_cache_delete('alloptions', 'options');
        foreach (glob(GUI_PATH . "/add-ons/*/plugin.xml") as $filename)
        {
            $addons = $this->__xml_parser($filename);
            if (count($this->__get_addons_preferences($addons)) > 2)
            {
                $external_addons[] = $addons;
            }
        }

        if (!is_array($external_addons))
        {
            $external_addons = array();
        }

        $this->list_addons = array_merge($this->default_addons, $external_addons);


        $notice = null;
        if (isset($_GET['action']))
        {
            if (!isset($_GET['addons']))
            {
                $_GET['addons'] = '';
            }

            $new_addons = wp_slash($this->__string_to_variable($_GET['addons']));

            $option = get_option("gui_addons");
            $new_option = $option;

            if (is_array($option['addons']))
            {
                $new_option_addons = $option['addons'];
            } else
            {
                $new_option_addons = array();
            }

            if ($_GET['action'] == 'activate')
            {
                if (!in_array($new_addons, $new_option_addons))
                {
                    $update = $this->__set_active_addons($new_addons);

                    if ($update == true)
                    {
                        $this->message = array(
                            'show' => true,
                            'level' => 'updated',
                            'text' => __('Add ons successfully activated', $this->textdomain));
                        $this->__admin_notices();

                    } else
                    {

                        $this->message = array(
                            'show' => true,
                            'level' => 'update-nag',
                            'text' => __('Add ons failed activated', $this->textdomain));
                        $this->__admin_notices();
                    }

                }
            } elseif ($_GET['action'] == 'deactivate')
            {

                $update = $this->__set_deactive_addons($new_addons);

                if ($update == true)
                {
                    $this->message = array(
                        'show' => true,
                        'level' => 'updated',
                        'text' => __('Add ons successfully deactivated, this will cause some of the toolbar does not work, please check <strong>the toolbar menu</strong>.', $this->textdomain));
                    $this->__admin_notices();
                } else
                {

                    $this->message = array(
                        'show' => true,
                        'level' => 'update-nag',
                        'text' => __('Add ons failed deactivated', $this->textdomain));
                    $this->__admin_notices();
                }
            }

        }


        //bulk aktivation
        if (isset($_POST['action']))
        {
            if (isset($_POST['checked']))
            {
                if (is_array($_POST['checked']))
                {
                    $checkeds = $_POST['checked'];

                    switch ($_POST['action'])
                    {
                        case 'deactivate-selected':
                            $t = 0;
                            foreach ($checkeds as $checked)
                            {

                                $update = $this->__set_deactive_addons($checked);
                                if ($update == true)
                                {
                                    $t++;
                                }
                            }
                            $this->message = array(
                                'show' => true,
                                'level' => 'updated',
                                'text' => $t . ' ' . __('add ons successfully deactivated.', $this->textdomain));
                            $this->__admin_notices();
                            break;
                        case 'activate-selected':
                            $t = 0;
                            foreach ($checkeds as $checked)
                            {

                                $update = $this->__set_active_addons($checked);
                                if ($update == true)
                                {
                                    $t++;
                                }
                            }
                            $this->message = array(
                                'show' => true,
                                'level' => 'updated',
                                'text' => $t . ' ' . __('add ons successfully activated.', $this->textdomain));
                            $this->__admin_notices();
                            break;
                    }


                }
            }
        }

        foreach ($this->default_active_addons as $default_addons)
        {
            $update = $this->__set_active_addons($default_addons);
        }

        $this->gui_addons("all");
        if (!isset($_GET['addons_status']))
        {
            $_GET['addons_status'] = 'all';
        }

        $active_addons = $this->gui_addons("active");
        $inactive_addons = $this->gui_addons("inactive");
        $all_addons = $this->gui_addons("all");

        $class_active = '';
        $class_inactive = '';
        $class_all = '';
        $current_page = 'addons_status=all';

        switch ($_GET['addons_status'])
        {
            case 'all':
                $list_addons = $all_addons;
                $class_all = 'current';
                $current_page = 'addons_status=all';
                break;
            case 'active':
                $list_addons = $active_addons;
                $class_active = 'current';
                $current_page = 'addons_status=active';
                break;
            case 'inactive':
                $list_addons = $inactive_addons;
                $class_inactive = 'current';
                $current_page = 'addons_status=inactive';
                break;
            default:
                $list_addons = $all_addons;
                $class_all = 'current';
                $current_page = 'addons_status=all';
                break;
        }


        $html_tables = null;

        $html_tables .= '    
        <table class="wp-list-table widefat plugins"> 
            <thead> 
                    <tr> 
                        <td id="cb" class="manage-column column-cb check-column">
                            <label class="screen-reader-text" for="cb-select-all-1">' . __('Select All', $this->textdomain) . '</label>
                            <input id="cb-select-all-1" type="checkbox" />
                        </td>
                        <th>' . __('Add Ons', $this->textdomain) . '</th>
                        <th>' . __('Description', $this->textdomain) . '</th>
                        <th>' . __('License', $this->textdomain) . '</th>
                    </tr>
            </thead>
            <tbody>
        ';
        $frameworks = array();
        $z = 0;
        foreach ($list_addons as $addons)
        {
            $addons_id = $this->__get_addons_preferences($addons);
            if (isset($addons_id['id']))
            {

                $addons_prefix = $addons_id['id'];
                $addons_name = $addons['name'];
                $addons_framework = '&nbsp;';

                if (isset($addons_id['framework']))
                {
                    $addons_framework = __("Framework", $this->textdomain) . ' : <ins>' . $addons_id['framework'] . '</ins>';
                } else
                {
                    $addons_id['framework'] = 'default';
                }
                $class_framework = str_replace(array(
                    '-',
                    '.',
                    ' '), '_', $addons_id['framework']);

                $label_framework = ucwords(str_replace(array(
                    '-',
                    '.',
                    '_'), ' ', $addons_id['framework']));

                $frameworks[$addons_id['framework']] = array('label' => $label_framework, 'value' => $class_framework);

                $addons_auhtor = $addons['author'];
                $addons_auhtor_url = $addons['authorURL'];
                $addons_version = $addons['version'];

                $option = get_option("gui_addons");
                $addons_active = 'inactive';

                if (isset($option['addons']))
                {
                    if (isset($option['addons'][$addons_prefix]))
                    {
                        $addons_active = 'active';
                    }
                }
                if (is_array($addons))
                {
                    $addons_license = '<span>-</span>';
                    if (isset($addons['license']))
                    {
                        if (!is_array($addons['license']))
                        {
                            $addons_license = '<span>' . ucwords($addons['license']) . '</span>';
                        }
                    }


                    if ($addons_active == "inactive")
                    {
                        $action_link = "admin.php?page=gui_settings&sub=addons&" . $current_page . "&action=activate&addons=" . $addons_prefix;
                        $class_name = "activate";
                        $action_text = __('Active', $this->textdomain);
                    } else
                    {
                        $action_link = "admin.php?page=gui_settings&sub=addons&" . $current_page . "&action=deactivate&addons=" . $addons_prefix;
                        $class_name = "deactivate";
                        $action_text = __('Deactive', $this->textdomain);
                    }
                    $html_tables .= '
                <tr id="' . $addons_prefix . '" class="' . $addons_active . ' row-' . $class_framework . '">
                
                    <th class="check-column" scope="row">
                        <label class="screen-reader-text" for="checkbox_' . $addons_prefix . '">
        					Select ' . $addons_name . '
        				</label>
                        <input type="checkbox" class="checkbox-' . $addons_id['framework'] . '" name="checked[' . $z . ']" id="checkbox_' . $addons_prefix . '" value="' . $addons_prefix . '"/>
                    </th>
                    <td class="plugin-title column-primary">
                        <strong>' . $addons_name . '</strong>
                        <p><em>' . $addons_framework . '</em></p>
                        <div class="row-actions visible">
        					<span class="' . $class_name . '">
        						<a class="button" href="' . $action_link . '" aria-label="' . $action_text . ' ' . $addons_name . '">' . $action_text . '</a>
        					</span>
        				</div>
                
                        <button type="button" class="toggle-row">
        					<span class="screen-reader-text">
        						' . __('Show more details', $this->textdomain) . '
        					</span>
        				</button>
                
                    </td>
                    <td class="column-description desc">
                        <div class="plugin-description">
                            <p>' . $addons['description'] . '</p>
                            
                        </div>
                        <div class="' . $addons_active . ' second plugin-version-author-uri">
        					' . __("Version", $this->textdomain) . ' ' . $addons_version . ' | ' . __("By", $this->textdomain) . '
        					<a href="' . $addons_auhtor_url . '">' . $addons_auhtor . '</a>
        					|
                            <a href="http://visual-editor.com/api/wp.php?addons=' . $addons_prefix . '" class="thickbox" title="' . __('Preview Add Ons ', $this->textdomain) . ' ' . $addons_name . ' ">' . __('View Detail', $this->textdomain) . '</a>
                            
        				</div>
                    </td>
                    <td class="column-license license">
                        <p><span class="label label-default">' . $addons_license . '</span></p>
                    </td>
                </tr>
                ';
                    $z++;
                }
            }
        }


        $html_tables .= '
        </tbody> 
        <tfoot> 
                <tr> 
                    <td id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-all-1">' . __('Select All', $this->textdomain) . '</label>
                        <input id="cb-select-all-1" type="checkbox" />
                    </td>
                    <th>' . __('Add Ons', $this->textdomain) . '</th>
                    <th>' . __('Description', $this->textdomain) . '</th>
                    <th>' . __('License', $this->textdomain) . '</th>
                    </tr>
        </tfoot>
 </table>';

        $html_framwork = '<div id="gui_filter_addons">';
        $html_framwork .= '<label>' . __('Framework', $this->textdomain) . ' :</label> ';
        foreach ($frameworks as $framework)
        {
            $html_framwork .= '<label><input type="checkbox" checked="checked" class="gui_filter_addons" value="' . $framework['value'] . '"/>' . ucwords($framework['label']) . '</label> ';
        }
        $html_framwork .= '</div>';

        $html .= '
<ul class="subsubsub">
	<li class="all"><a href="admin.php?page=gui_settings&sub=addons&addons_status=all" class="' . $class_all . '" >' . __("All", $this->textdomain) . ' <span class="count">(' . count($all_addons) . ')</span></a> |</li>
	<li class="active"><a href="admin.php?page=gui_settings&sub=addons&addons_status=active" class="' . $class_active . '">' . __("Active", $this->textdomain) . ' <span class="count">(' . count($active_addons) . ')</span></a> |</li>
	<li class="inactive"><a href="admin.php?page=gui_settings&sub=addons&addons_status=inactive" class="' . $class_inactive . '">' . __("Inactive", $this->textdomain) . ' <span class="count">(' . count($inactive_addons) . ')</span></a></li>
</ul>

<form method="post" action="admin.php?page=gui_settings&sub=addons" id="bulk-action-form">

    <div class="tablenav top">         
        <div class="alignleft actions bulkactions">
        	<label for="bulk-action-selector-top" class="screen-reader-text">' . __('Select bulk action', $this->textdomain) . '</label>
        	<select name="action" id="bulk-action-selector-top">
        		<option value="-1" selected="selected">' . __('Bulk Actions', $this->textdomain) . '</option>
        		<option value="activate-selected">' . __('Activate', $this->textdomain) . '</option>
        		<option value="deactivate-selected">' . __('Deactivate', $this->textdomain) . '</option>
        	</select>
        	<input type="submit" id="doaction" class="button action" value="' . __('Apply', $this->textdomain) . '"  />
        </div>      
    </div>
    ';
        $html .= $html_framwork;
        $html .= $html_tables;
        $html .= '
    <div class="tablenav bottom">
        <div class="alignleft actions bulkactions">
        	<label for="bulk-action-selector-bottom" class="screen-reader-text">' . __('Select bulk action', $this->textdomain) . '</label>
        	<select name="action2" id="bulk-action-selector-bottom">
        		<option value="-1" selected="selected">' . __('Bulk Actions', $this->textdomain) . '</option>
        		<option value="activate-selected">' . __('Activate', $this->textdomain) . '</option>
        		<option value="deactivate-selected">' . __('Deactivate', $this->textdomain) . '</option>
        	</select>
        	<input type="submit" id="doaction2" class="button action" value="' . __('Apply', $this->textdomain) . '"  />
        </div>
     </div>
            
 </form> 
  ';

        return $html;
    }


    /**
     * GuiVisualEditor::__manage_toolbar()
     * 
     * @return void
     */
    function __manage_toolbar()
    {
        echo ' 
                    <div class="wrap">
                        <h2 class="nav-tab-wrapper">
                            <a href="?page=gui_settings&sub=editor" class="nav-tab">' . __('Editor', $this->textdomain) . '</a>
                            <a href="?page=gui_settings&sub=addons" class="nav-tab">' . __('Add Ons', $this->textdomain) . '</a>
                            <a href="?page=gui_settings&sub=toolbars" class="nav-tab nav-tab-active">' . __('Toolbars', $this->textdomain) . '</a>
                        </h2>
             
                                          
                        <div class="addons-liquid-left">
                            <div id="addonss-left">
                            
                            	<div id="available-addonss" class="addonss-holder-wrap">
                                
                            		<div class="sidebar-name">
                            			<div class="sidebar-name-arrow"><br /></div>
                            			<h3>' . __('Editor toolbar available', $this->textdomain) . '<span id="removing-addons">' . __('Deactivate', $this->textdomain) . ' <span></span></span></h3>
                            		</div>
                                    
                            		<div class="addons-holder">
                            			<div class="sidebar-description">
                            				<p class="description">' . __("To activate a editor toolbar drag it to a sidebar or click on it. To deactivate a toolbar editor and delete its settings, drag it back.", $this->textdomain) . '</p>
                            			</div>
                            			<div id="addons-list">
                            				' . $this->__addons_available_list() . '
                            			</div>
                            			<br class="clear" />
                            		</div>
                            		<br class="clear" />    
                            	</div>
                             </div>
                        </div>
                        
                        <div class="addons-liquid-right">
                            <div id="addonss-right">
                                <div class="sidebars-column-1">
                                    <div class="addonss-holder-wrap widgets-holder-wrap" >  
                                        <div id="first_area" class="addonss-sortables ui-droppable ui-sortable">		
                                            
                                            <div class="sidebar-name">
                                            	<div class="sidebar-name-arrow"><br></div>
                                            	<h3>' . __('The first line toolbar', $this->textdomain) . ' <span class="spinner"></span></h3>
                                            </div>
                                            <div class="sidebar-description">
                                                <p class="description">' . __('The toolbar area', $this->textdomain) . '</p>
                                            </div>
                                            ' . $this->__addons_usage_list("first_area") . '
                                        </div>
                                    </div>
                                    
                                    <div class="addonss-holder-wrap widgets-holder-wrap" >  
                                        <div id="second_area" class="addonss-sortables ui-droppable ui-sortable">		
                                            
                                            <div class="sidebar-name">
                                            	<div class="sidebar-name-arrow"><br></div>
                                            	<h3>' . __('The second line toolbar', $this->textdomain) . ' <span class="spinner"></span></h3>
                                            </div>
                                            <div class="sidebar-description">
                                                <p class="description">' . __('The toolbar area', $this->textdomain) . '</p>
                                            </div>
                                            ' . $this->__addons_usage_list("second_area") . '
                                        </div>
                                    </div>
                                     
                                </div>
                        
                            <div class="sidebars-column-2">     
                              
                                    <div class="addonss-holder-wrap widgets-holder-wrap" >  
                                        <div id="third_area" class="addonss-sortables ui-droppable ui-sortable">		
                                            <div class="sidebar-name">
                                            	<div class="sidebar-name-arrow"><br></div>
                                            	<h3>' . __('The third line toolbar', $this->textdomain) . ' <span class="spinner"></span></h3>
                                            </div>
                                            <div class="sidebar-description">
                                                <p class="description">' . __('The toolbar area', $this->textdomain) . '</p>
                                            </div>
                                            ' . $this->__addons_usage_list("third_area") . '
                                        </div>
                                    </div>
                                    
                                    <div class="addonss-holder-wrap widgets-holder-wrap" >  
                                        <div id="fourth_area" class="addonss-sortables ui-droppable ui-sortable">		
                                            
                                            <div class="sidebar-name">
                                            	<div class="sidebar-name-arrow"><br></div>
                                            	<h3>' . __('The fourth line toolbar', $this->textdomain) . ' <span class="spinner"></span></h3>
                                            </div>
                                            <div class="sidebar-description">
                                                <p class="description">' . __('The toolbar area', $this->textdomain) . '</p>
                                            </div>
                                          ' . $this->__addons_usage_list("fourth_area") . '  
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    
                </div>
                
                <div class="addonss-chooser">
                    	<ul class="addonss-chooser-sidebars"></ul>
                    	<div class="addonss-chooser-actions">
                    		<button class="button-secondary">' . __('Cancel') . '</button>
                    		<button class="button-primary">' . __('Add addons') . '</button>
                    	</div>
                </div>
                    ';
    }

    /**
     * Get reguest data from Ajax
     *
     * @access public
     * @return void
     *
     **/
    public function gui_ajax_addons_save()
    {
        $button = json_decode(get_option("gui_button_list"), true);
        if (!is_array($button))
        {
            $button = array();
        }

        $key = sanitize_text_field(wp_unslash($_POST['addons-id']));
        $addons_id = sanitize_text_field(wp_unslash($_POST['addons-idbase']));
        $addons_button = sanitize_text_field(wp_unslash($_POST['addons-button']));
        $addons_default = sanitize_text_field(wp_unslash($_POST['addons-default']));
        $addons_title = sanitize_text_field(wp_unslash($_POST['addons-title']));
        $addons_desc = sanitize_text_field(wp_unslash($_POST['addons-description']));

        $button[$key]['id'] = $key;
        $button[$key]['idbase'] = $addons_id;
        $button[$key]['button'] = $addons_button;
        $button[$key]['default'] = $addons_default;
        $button[$key]['description'] = $addons_desc;
        $button[$key]['title'] = $addons_title;

        update_option("gui_button_list", json_encode($button));

        $new_button = json_decode(get_option("gui_button_list"), true);
        $html = '
        
        <div class="addons-content widget-content">
			<p>
				<label>' . __("Code", $this->textdomain) . '</label>
				<textarea class="widefat addons-button" name="addons-button" >' . esc_attr($new_button[$key]['button']) . '</textarea>
            </p>                      
         </div>

        ';

        echo $html;
        die();
    }
    /**
     * GuiVisualEditor::gui_ajax_save_post()
     * 
     * @return
     */
    function gui_ajax_save_post()
    {

        $postID = intval($_POST["post_id"]);
        if (empty($postID))
        {
            return false;
        }
        if (!current_user_can('edit_post', $postID))
        {
            return false;
        }

        $post_update = wp_update_post(array("ID" => $postID, "post_content" => $_POST["post_content"]));
        echo '1';
        die($post_update);
    }
    /**
     * Get reguest data from Ajax
     *
     * @access public
     * @return void
     *
     **/
    public function gui_ajax_addons_order()
    {
        $postdata = array();
        foreach ($_POST['sidebars'] as $key => $val)
        {
            $postdata[sanitize_text_field(wp_unslash($key))] = sanitize_text_field(wp_unslash($val));
        }
        update_option("gui_button_order", json_encode($postdata));

        $addons_used = array();
        foreach ($postdata as $data)
        {
            foreach (explode(',', $data) as $addons)
            {
                if (!empty($addons))
                {
                    $addons_used[] = trim($addons);
                }
            }
        }
        $new_button = array();
        $cur_button = json_decode(get_option("gui_button_list"), true);
        foreach (array_keys($cur_button) as $key)
        {
            if (in_array($key, $addons_used))
            {
                $new_button[$key] = $cur_button[$key];
            }
        }
        update_option("gui_button_list", json_encode($new_button));
        die(); //required
    }

    /**
     * Create array from xml data
     * 
     * @access public
     * @return void
     **/
    private function __xml_parser($file)
    {
        $xml = array();
        if (file_exists($file))
        {
            $xml = json_decode(json_encode(simplexml_load_file($file)), true);
        } else
        {
            $xml = array();
        }
        return $xml;
    }
    /**
     * Get All framework support
     * 
     * @return void
     */
    function __refresh_framework_supported()
    {
        $css_registered = $css_supported = $font_family = array();
        global $wp_styles;
        if (isset($wp_styles->registered))
        {
            foreach ($wp_styles->registered as $css)
            {
                $css_registered[] = $css->src;
            }
        }
        $css = null;

        $z = 0;
        foreach ($this->framework_list as $framework)
        {
            foreach ($css_registered as $css)
            {
                $pattern = "/" . $framework['pattern'] . "/i";
                if (preg_match($pattern, $css))
                {
                    $css_supported[$z]['name'] = $framework['name'];
                    $css_supported[$z]['src'] = $css;

                    if ($this->__get_info_googlefont($css) != null)
                    {
                        $font_family[] = $this->__get_info_googlefont($css);
                    }

                    $z++;
                }


            }
        }
        $z = 0;
        foreach ($css_registered as $css)
        {
            if (preg_match("/wp-content\/themes/i", $css))
            {
                $css_themes[$z]['name'] = md5($css);
                $css_themes[$z]['src'] = $css;
                $z++;
            }

        }


        $googlefont = array_unique($font_family);

        update_option("gui_framework_googlefont", json_encode($googlefont));
        update_option("gui_framework_available", json_encode($css_supported));
        update_option("gui_css_themes", json_encode($css_themes));

    }
    /**
     * Retrieved data
     *
     * @access public
     * @param mixed $content
     * @return void
     * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/the_content
     **/

    public function gui_the_content($content)
    {
        global $post;

        $this->__refresh_framework_supported();

        $new_content = null;
        if ($this->live_edit == true)
        {
            remove_all_shortcodes();
            $new_content .= '
            <div id="gui-frontend-barner"></div>
            <form>
            <input type="hidden" id="gui-content" name="gui-content" value="' . $post->ID . '" />
            <div id="gui-editable">' . $content . '</div>
            </form>
            ';
        } else
        {
            $new_content .= $content;
        }
        return $new_content;
    }

    /**
     * TinyMCE Setup Toolbar
     * 
     * @return
     */
    function __setup_tinymce_toobar($var)
    {
        $menubar = array();
        $buttons_list = json_decode(get_option("gui_button_list"), true);
        if (!is_array($buttons_list))
        {
            $buttons_list = array();
        }

        $buttons_order = json_decode(get_option("gui_button_order"), true);
        if (!is_array($buttons_order))
        {
            $buttons_order = array();
        }


        if (isset($buttons_order[$var]))
        {
            foreach (explode(',', $buttons_order[$var]) as $addons)
            {
                if (!empty($addons))
                {
                    $addons_used = trim($addons);
                    if (isset($buttons_list[$addons_used]))
                    {

                        if (isset($buttons_list[$addons_used]['button']))
                        {
                            $menubar[] = $buttons_list[$addons_used]['button'];
                        }
                    }
                }
            }
        }

        return $menubar;

    }

    /**
     * Get google font info
     * 
     * @param mixed $url
     * @return
     */
    private function __get_info_googlefont($url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $output);
        if (count($output) != 0)
        {
            if (isset($output['family']))
            {
                $family = explode(':', $output['family']);
            }
            return $family[0];
        } else
        {
            return null;
        }
    }


    /**
     * Insert Dinamic JS
     * @param object $hooks
     * @access public
     * @return void
     **/
    public function gui_ajax_tinymce_setup()
    {
        header("Content-type: application/javascript");


        $tinymce_content_css = $tinymce_fontfamily = $list_css = '';

        $tinymce_option = null;
        $ready_default = array();
        $plugin_tinymce = $tinymce_css = $ext_plugin = $tinymce_font = array();

        //include css
        $framework_availables = json_decode(get_option("gui_framework_available"), true);

        if (is_array($framework_availables))
        {
            foreach ($framework_availables as $framework_available)
            {
                $url_css = str_replace(array("http://", "https://"), "//", $framework_available['src']); // remove http/https
                $url_css = str_replace(",", "%2c", $url_css); //tinymce use coma as separator so we must avoid using coma.
                $tinymce_css[] = '"' . $url_css . '"';
                if ($framework_available['name'] != 'Google Fonts')
                {
                    $list_css .= "\r\n\t\t" . 'gui_' . $this->__string_to_variable($framework_available['name']) . '_url ' . "\t" . '= "' . $url_css . '";';
                }
            }
        }

        $gui_css_themes = json_decode(get_option("gui_css_themes"), true);
        foreach ($gui_css_themes as $gui_css_theme)
        {
            $url_css = str_replace(array("http://", "https://"), "//", $gui_css_theme['src']); // remove http/https
            $url_css = str_replace(",", "%2c", $url_css); //tinymce use coma as separator so we must avoid using coma.

            $tinymce_css[] = '"' . $url_css . '"';
        }
        $tinymce_css = array_unique($tinymce_css);


        if (is_array($tinymce_css))
        {
            $tinymce_content_css .= "\r\n\t\t\t\t" . 'gui_css: [';
            $tinymce_content_css .= "\r\n\t\t\t\t\t\t\t" . implode(",\r\n\t\t\t\t\t\t\t", $tinymce_css);
            $tinymce_content_css .= "\r\n\t\t\t\t" . '],';
        }
        //include fontfamily
        $framework_googlefonts = json_decode(get_option("gui_framework_googlefont"), true);
        if (is_array($framework_googlefonts))
        {
            foreach ($framework_googlefonts as $framework_googlefont)
            {
                $tinymce_font[] = $framework_googlefont . "='" . $framework_googlefont . "';";
            }
        }

        if (is_array($tinymce_css))
        {
            $tinymce_fontfamily .= "\r\n\t\t\t\t" . 'font_formats:';
            $tinymce_fontfamily .= "\r\n\t\t\t\t\t\t" . '"' . implode("\" + \r\n\t\t\t\t\t\t\"", $tinymce_font) . '",';
        }

        //include plugin
        $option = get_option("gui_addons");
        foreach ($option['addons'] as $addons)
        {
            $plugin_data = json_decode($addons, true);

            $plugin_tinymce = $this->__get_addons_preferences($plugin_data);

            if (isset($plugin_tinymce['id']))
            {
                if (isset($plugin_tinymce['framework']))
                {
                    $var_css_list[md5($plugin_tinymce['framework'])] = 'gui_' . $plugin_tinymce['framework'] . '_url';
                }

                if (!isset($plugin_data['default']))
                {
                    $plugin_data['default'] = null;
                }
                if ($plugin_data['default'] == false)
                {
                    if (preg_match("/http/", $plugin_tinymce['path']))
                    {
                        $ext_plugin[] = "\r\n\t\t\t\t\t\t\t" . '"' . $plugin_tinymce['id'] . '":"' . $plugin_tinymce['path'] . '"';
                    } else
                    {
                        $ext_plugin[] = "\r\n\t\t\t\t\t\t\t" . '"' . $plugin_tinymce['id'] . '":"' . GUI_URL . '/' . $plugin_tinymce['path'] . '"';
                    }

                } else
                {
                    if ($plugin_tinymce['id'] != "core")
                    {
                        $ready_default[] = $plugin_tinymce['id'];
                    }
                }
            }

            $basic_param = array(
                "menu",
                "id",
                "button",
                "path",
                "framework",
                "license");
            foreach (array_keys($plugin_tinymce) as $key)
            {
                if (!in_array($key, $basic_param))
                {
                    if ($key != 'gui_css')
                    {
                        $tinymce_option .= "\r\n\t\t\t\t\t" . $key . ':{toolbar_text:true,css:{exist:true}},';
                    }
                }

            }

        }

        //external plugin
        $external_plugins = "\r\n\t\t\t\t" . 'external_plugins: {' . implode(",", $ext_plugin) . "\r\n\t\t\t\t" . '},';


        $menubar = 'menubar:false,';
        if (isset($this->options["menubar"]))
        {
            if ($this->options["menubar"] == true)
            {
                $menubar = 'menubar:true,';
            }
        }

        $inline = 'inline:false,';
        if (isset($this->options["inline_editor"]))
        {
            if ($this->options["inline_editor"] == true)
            {
                $inline = 'inline:true,';
            }
        }

        //toolbar
        $toolbar1 = $toolbar2 = $toolbar3 = $toolbar4 = null;
        if (count($this->__setup_tinymce_toobar('first_area')) != 0)
        {
            $toolbar1 = 'toolbar1: "save ' . implode(' ', $this->__setup_tinymce_toobar('first_area')) . '",';
        } else
        {
            $toolbar1 = 'toolbar1: "save | undo redo | styleselect fontselect | bold italic | alignleft aligncenter alignright alignjustify | image link table",';
        }

        if (count($this->__setup_tinymce_toobar('second_area')) != 0)
        {
            $toolbar2 = 'toolbar2: "' . implode(' ', $this->__setup_tinymce_toobar('second_area')) . '",';
        }

        if (count($this->__setup_tinymce_toobar('third_area')) != 0)
        {
            $toolbar3 = 'toolbar3: "' . implode(' ', $this->__setup_tinymce_toobar('third_area')) . '",';
        }

        if (count($this->__setup_tinymce_toobar('fourth_area')) != 0)
        {
            $toolbar4 = 'toolbar4: "' . implode(' ', $this->__setup_tinymce_toobar('fourth_area')) . '",';
        }
        if (!isset($var_css_list))
        {
            $var_css_list = array('gui_default_url');
        }
        if (!is_array($var_css_list))
        {
            $var_css_list = array('gui_default_url');
        }
        // Set alway active plugin
        foreach ($this->default_active_addons as $active_addons)
        {
            if ($active_addons != 'core')
            {
                $alway_active_addons[] = $active_addons;
            }

        }

        $default_active_plugins = implode(' ', $alway_active_addons);

        $save_code = 'tinymce.activeEditor.windowManager.alert("' . __('You are not allowed to edit this pages', $this->textdomain) . '");';


        if (is_admin())
        {
            $save_code = '
                    var gui_content =  editor.getContent();
                    var gui_postid =  $("#gui-content").val();
                    $.ajax({
                        type    : "POST",
                        cache   : false,
                        url     : "' . admin_url() . '/admin-ajax.php?action=save_post",
                        data    : {post_content:gui_content,post_id:gui_postid},
                        success : function(result){
                                if(result == "0"){ 
                                    tinymce.activeEditor.windowManager.alert("' . __('You are not allowed to edit this pages', $this->textdomain) . '");
                                }else{
                                    tinymce.activeEditor.windowManager.alert("' . __('This page has been successfully saved', $this->textdomain) . '");
                                }
                        }
                    });
            ';
        }

        echo '(function($){
        var ' . implode(',', $var_css_list) . '="";
        
        ' . $list_css . '
        
        tinymce.init({
                selector:"#gui-editable",
                theme: "modern",
                skin: "galau_ui",
                ' . $menubar . '
                ' . $inline . '
                plugins :"' . $default_active_plugins . ' ' . implode(',', $ready_default) . '",
                ' . $tinymce_content_css . '
                ' . $external_plugins . '
                ' . $tinymce_fontfamily . '
                add_unload_trigger: false,
                ' . $toolbar1 . '
                ' . $toolbar2 . '
          		' . $toolbar3 . '
                ' . $toolbar4 . '
                
                save_onsavecallback: function(editor){ 
                    ' . $save_code . '
                },
                save_oncancelcallback: function(){ 
                    console.log("Save canceled"); 
                },
        		pagebreak_separator: "<!-- more -->",
                file_browser_callback: function(field_name, url, type, win) {
                    if(gui_media_upload) {
                      gui_media_upload.open();
                      return;
                    }
                    
                    var gui_media_upload = wp.media({
                      id        : "gui_visualeditor_media",   
                      title     : "Select or Upload Media Of Your Chosen Persuasion",
                      button    : {text:"Use this media"},
                      multiple  : false
                    });
                    
                    gui_media_upload.on("select",function(){
                        var attachment = gui_media_upload.state().get("selection").first().toJSON();
                        var url = attachment.url ;
                        win.document.getElementById(field_name).value = url;
                    });
                    
                    gui_media_upload.open();
            		return false;
            	},  
                contextmenu: "gui_advtoolbars gui_bs_quicktags gui_fi_quicktags gui_bs_visualblocks gui_bs_glyphicons gui_dashicons gui_fontawesome | link image inserttable | cell row column deletetable",
                ' . $tinymce_option . '
                
                         
            });  
            
})(jQuery); 
            ';

        die();
    }


    /**
     * Convert string to variable
     * 
     * @param mixed $string
     * @return
     */
    private function __string_to_variable($string)
    {
        $char = 'abcdefghijklmnopqrstuvwxyz_12345678900';
        $Allow = null;
        $string = str_replace(array(
            ' ',
            '-',
            '__'), '_', strtolower($string));
        $string = str_replace(array('___', '__'), '_', strtolower($string));
        for ($i = 0; $i < strlen($string); $i++)
        {
            if (strstr($char, $string[$i]) != false)
            {
                $Allow .= $string[$i];
            }
        }
        return $Allow;
    }

    /**
     * Generate code for admin notice
     * 
     * @return
     */
    private function __admin_notices()
    {
        if ($this->message['show'] == true)
        {
            echo '<div class="' . $this->message['level'] . ' notice is-dismissible"><p>' . $this->message['text'] . '</p></div>';
        }
    }

    /**
     * Get list Adds Ons
     * 
     * @param string $type
     * @return void
     */
    private function gui_addons($type = "all")
    {
        $all_addons = $active_addons = $inactive_addons = array();
        $option = get_option("gui_addons");
        foreach ($this->list_addons as $addons)
        {
            $old_addons = $addons;
            if (isset($option['addons']))
            {
                $addons_preferences = $this->__get_addons_preferences($addons);
                $addons_id = $addons_preferences['id'];
                if (!isset($option['addons'][$addons_id]))
                {
                    $option['addons'][$addons_id] = '';
                }
                if ($option['addons'][$addons_id])
                {
                    $old_addons['active'] = true;
                    $active_addons[] = $old_addons;
                } else
                {
                    $old_addons['active'] = false;
                    $inactive_addons[] = $old_addons;
                }

            }
            $all_addons[] = $old_addons;
        }

        //update_option("gui_addons",'');

        switch ($type)
        {
            case "all":
                return $all_addons;
                break;
            case "active":
                return $active_addons;
                break;
            case "inactive":
                return $inactive_addons;
                break;
        }
    }

    /**
     * Get addons preferences
     * 
     * @param mixed $plugin_data
     * @return void
     */
    private function __get_addons_preferences($plugin_data)
    {
        $plugin_tinymce = array();
        if (isset($plugin_data['preferences']['preference']))
        {
            foreach ($plugin_data['preferences']['preference'] as $preference)
            {
                if (isset($preference['@attributes']))
                {
                    if (isset($preference['@attributes']['name']))
                    {
                        $key = $preference['@attributes']['name'];
                    }
                    if (isset($preference['@attributes']['value']))
                    {
                        $value = $preference['@attributes']['value'];
                    }
                    $plugin_tinymce[$key] = $value;
                }
            }

        }


        return $plugin_tinymce;
    }
    /**
     * Get addons info
     * 
     * @param mixed $id
     * @return void
     */
    private function __addons_info($id)
    {
        $new_addons = array();
        foreach ($this->list_addons as $addons)
        {
            $addons_preferences = $this->__get_addons_preferences($addons);
            $current_id = $addons_preferences['id'];
            $new_addons[$current_id] = $addons;
        }
        if (isset($new_addons[$id]))
        {
            return $new_addons[$id];
        } else
        {
            return null;
        }
    }

}


new GuiVisualEditor();
register_activation_hook(__file__, array("GuiVisualEditor", "gui_activation"));
register_deactivation_hook(__file__, array("GuiVisualEditor", "gui_deactivation"));
