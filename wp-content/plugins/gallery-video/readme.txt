=== Video Gallery ===
Contributors: Huge-IT
Donate link: http://huge-it.com/wordpress-video-gallery/
Tags: video gallery, gallery, video, youtube, vimeo, wp gallery, media gallery, video plugin, vimeo gallery, youtube gallery, embed videos, youtube player, Video Player plugin, videos, wordpress gallery, grid gallery, wordpress youtube, wp gallery plugins, justified gallery, wordpress video, video slider, video slideshow, video player, player, video lightbox, flickr, free gallery, slideshow, Simple gallery, wordpress gallery plugin, thumbnail gallery, slideshow gallery, videos, plugin gallery, photo gallery, responsive gallery, WordPress Video Gallery, youtube video gallery, video gallery plugin, video youtube, add gallery, best gallery, filterable gallery, fullscreen gallery, free photo gallery, gallery decription, widget gallery, website gallery, jquery gallery, wp gallery plugin, best gallery plugin, gallery image, gallery lightbox, Gallery Plugin, gallery shortcode, gallery slider, sortable gallery, thumbnails, content gallery, lightbox gallery,
Requires at least: 3.0.1
Tested up to: 4.4.2
Stable tag: 1.6.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Video Gallery plugin was created and specifically designed to show your video files in unusual splendid ways.

== Description ==

[Wordpress Video Gallery](http://huge-it.com/wordpress-video-gallery/)  
[Demo](http://huge-it.com/wordpress-video-gallery-demo-1-content-popup/)  
[FAQ](http://huge-it.com/wordpress-video-gallery-faq/)  
[User Manual Video Gallery](http://huge-it.com/wordpress-video-gallery-user-manual/)  

https://www.youtube.com/watch?v=Re16ci9iGVU

Video Gallery plugin was created and specifically designed to show your videos from Vimeo and Youtube in unusual splendid ways. It has 5 good-looking views. Each are made in different taste so that you can choose any of them, according to the style of your website.

* Upload the video in your Video Gallery
* Wright a title
* Add description
* Give some link

Video gallery gives you the choice between 7 unrepeatable views. Tt has a view to show your videos with it’s information next to it in a beautiful popup, or the same view can be slide, in the second view, then you can choose to demonstrate  the videos in lightbox, or only slide the videos, in the next view, set up the way to slide and have fun, the last view is to show your videos in a little box which can be opened in lightbox and be slide at a time. So the choice are huge. You can provide all the information as from uploader panel, as after inserting in the Video Gallery panel.

Each of the view has changeable options in ‘General Options’ section of Video Gallery. Every detail is allowed to be change so that our users be satisfy of the results they get by using Video Gallery plugin. And everything are so easy to use that even your child can manage it. Make the Video shows. And demonstrate the content of your website in more kindly and pleasant way. 

The  Video Gallery allows you to add video from 2 kind of sources, it is Vimeo and Youtube.
Thumbnail image will come with the video automatically, but you can also upload a custom image for it.
While visitor will watch your video on the backend you can can learn how many times your videos has been watched.
This function is called View Counter. Plus, export view counter data as pdf file - Video Gallery
From time to time we will make all kind of updates, and add many other even better view to our Video Gallery, so that to be competitive among the other Video plugins in WordPress. So we have an aim to be the best Video Gallery and we will achieve our goal. Now it time for you to install and just try how nice it works!

### Video gallery 7 Views Options:

### Video Gallery / Content popup
This view has been made for showing your videos in popup with the information, that is given to the videos and the title, In the popup you can also find a Link button which takes to another page. The front image of your video will be settled be in compact box with zoom icon on it. Clicking on them and open the video in popup.

### Content video slider
This view will introduce your video with it’s description and title next to it, in a wonderful slider. Choose the way to change the slides,m and enjoy sliding each video with the content.

### Lightbox video gallery
This view shows only the videos, without any text in a boxes, which will open in lightbox while clicking on them, it is also possible to adjust the style of the lightbox, in Lightbox Options section.

### Video Slider
Show your videos in unique way by using Slider view. Here you can put the title and description straight on the video, choose the manner of sliding your videos, change styles and enjoy sliding your videos.

### Thumbnails
Breathe new life to your site, show videos using thumbnails view. Pick a background image for the video. Open them in Lightbox and enjoy the show.

### Justified View
Justified view of the Video Gallery allows you to represent your videos within your created gallery next to each other without padding between them. The interesting thing about this gallery type, is that it has “order changing” function, which you need to enable from General Options, and your videos will be changed in order while refreshing the page. By hovering on the items of the gallery, appears the title with background. Clicking on the video it opens in lightbox, giving a beautiful view to them.

### Blog Style View
Using Image Gallery with Block Style Gallery views is the simplest and most understanding way of demonstrating your content within gallery. This view is made to simply display your images/videos with it’s natural dimensions, with title above and description under it. Of course, the position of title and description can be changed from General Options of the Block Style Gallery view. Description allows to use html modified text, so you can beautifully create your projects and place them one under each other. The outstanding feature of this view, you can add pagination or load more symbol at the end of the gallery page, and this way divide your projects into pages.

### Video gallery Unlimited Amount Of Galleries Videos

Add unlimited number of videos in a single gallery.And as you created a number of video galleries, you can add as many gallery shortcodes in your page, as you need.

### Video gallery Fully Responsive

Video Gallery plugin is fully responsive for different sizes of screen. So your users will enjoy the gallery using it on any device.

### Video gallery Load More And Pagination

This feature will allow to demonstrate only a part of your created videos, hiding the rest of them under «load more» button, or dividing all your projects into several pages. You also choose how many projects to display per page.

### Video gallery Title And Description

Important possibility to add title and description to each video. Description can be seen not in all view, but some of them has a place for it. Image Gallery accomplished with title and description looks rich and perfect.

### Video gallery 7 Nicely Designed Views

7 ways to demonstrate videos will make you satisfy with the look of your gallery. Among the views you can find

* Gallery / content-popup
* Content Slider
* Ligthtbox - Gallery
* Huge-IT Slider
* Thumbnail View
* Justified View
* Blog Style View

### Video gallery Hundreds Of Design Options

General Options of Pro version, gives you big choice of different settings, make the gallery look exactly as you need will plenty of adjustments of colors, size, and effects.

### Video gallery Youtube Posts

Video Gallery can be used with the most popular video site -YouTube, Simply copy the link and add it to the Video Gallery gallery will bring the video in it. This way you can transform your gallery into YouTube Gallery

### Video gallery Vimeo Posts

The other source of adding videos in Video Gallery- Vimeo. Turn your gallery into Vimeo Gallery using your collection of vimeo videos. Inserting the links and making customization on it.

### Video gallery Lightbox Popup With Many Options

Some of the views use our popular Lightbox tool. Videos are opened using the Lightbox, or sometimes we say Popup. The idea of the Lightbox is to make Video larger, ability to slide through all videos withing the the gallery. And make some design customization on it, using varioty of Lightbox Options.

**[Video gallery demo 1 Content Popup](http://huge-it.com/wordpress-video-gallery-demo-1-content-popup/)**

Video gallery This view has been made for showing your videos in popup with the information, that is given to the videos and the title, In the popup you can also find a Link button which takes to another page. The front image of your video will be settled be in compact box with zoom icon on it. Clicking on them and open the video in popup on video gallery.

**[Video gallery demo 2 Content Video Slider](http://huge-it.com/wordpress-video-gallery-demo-2-content-video-slider/)**

This view will introduce your video with it’s description and title next to it, in a wonderful slider. Choose the way to change the slides,m and enjoy sliding each video with the content.

**[Video gallery demo 3 Lightbox video Gallery](http://huge-it.com/wordpress-video-gallery-demo-3-lightbox-video-gallery/)**

This view shows only the videos, without any text in a boxes, which will open in lightbox while clicking on them, it is also possible to adjust the style of the lightbox, in Lightbox Options section.


> #### Video Gallery Demos 
>
>[Content-Popup Video Gallery Demo](http://huge-it.com/wordpress-video-gallery-demo-1-content-popup/)
>
>[Content Video Slider Demo](http://huge-it.com/wordpress-video-gallery-demo-2-content-video-slider/)
>
>[Lightbox Gallery  Demo](http://huge-it.com/wordpress-video-gallery-demo-3-lightbox-video-gallery/)
>
>[Video Slider Demo](http://huge-it.com/wordpress-video-gallery-demo-4-video-slider/)
>
>[Thumbnails Demo](http://huge-it.com/wordpress-video-gallery-demo-5-thumbnails/)
>
>[Custom Thumbnails Demo](http://huge-it.com/wordpress-video-gallery-demo-6-custom-thumbnails/)
>
>[Blog Style View With pagination  Demo](http://huge-it.com/wordpress-video-gallery-demo-7-block-style-view/)

> #### Video Gallery FAQ & User Manual
>
>[FAQ](http://huge-it.com/wordpress-video-gallery-faq/)
>
>[User Manual Video Gallery](http://huge-it.com/wordpress-video-gallery/)

Upgrade to [WordPress Video Gallery Pro](http://huge-it.com/wordpress-video-gallery/) to Customizable Styles and Colors.

If you think, that you found a bug in our [WordPress Video Gallery](http://huge-it.com/wordpress-video-gallery/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com)

### Wordpress Video Gallery

== Installation ==

### First download the ZIP file ,

1. Log in to your website administrator panel.
2. Go to Plugins page, and add new plugin.
3. Upload [WordPress Video Gallery](https://wordpress.org/plugins/gallery-video/). 
4. Click `Install Now` button.
5. Then click `Activate Plugin` button.

Now you can set your Video Gallery options, images and use our Video Gallery.

If you think, that you found a bug in our [WordPress Video Gallery](http://huge-it.com/wordpress-video-gallery/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com)

== Screenshots ==

1.  WordPress Video Gallery view 1 - Video Gallery / Content Popup
11.  WordPress Video Gallery view 1 - Video Gallery / Content Popup
2.  WordPress Video Gallery view 2 - Content Video Slider
3.  WordPress Video Gallery view 3 - Lightbox Video Gallery
32.  WordPress Video Gallery view 3 - Lightbox Video Gallery
4.  WordPress Video Gallery view 4 - Video Slider
5.  WordPress Video Gallery view 5 - Video Gallery - Thumbnails
51.  WordPress Video Gallery view 5 - Video Gallery - Thumbnails
52.  WordPress Video Gallery view 5 - Video Gallery - Thumbnails
53.  WordPress Video Gallery view 6 - Video Gallery - Custom Thumbnails
54.  WordPress Video Gallery view 7 - Video Gallery - Block Style View
6.  WordPress Video Gallery Admin Page - Video Gallery
62.  WordPress Video Gallery Admin Page - Video Gallery
63.  WordPress Video Gallery Admin Page - Lightbox video gallery Options
64.  WordPress Video Gallery Admin Page - Video Gallery - Lightbox Options


== Frequently Asked Questions ==

### Video Gallery

**How to get a copy of most recent version?**
Pro users can get update versions contacting us by info@huge-it.com.
Free version users will find update notification in their wordpress admin panel.

**I have paid for pro version video gallery and didn’t get the link or file to update.**
If you made purchase and didn’t get the file, or file was corrupt, contact us by info@huge-it.com and send “order number”, we will check and send you the file as soon as possible

**Have purchased pro version still get the announcement to buy the commercial version to change settings. What to do?**
This can happen because of your browser’s cache files. Press ctrl+f5 (Chrome, FF) in order to clean them, if you use safari, etc., clean from browser settings in video gallery.

**Will I lose all my changes that I made in free version, if I update to video gallery pro version?**
All kind of changes made in free version will remain, even if you delete the plugin Video Gallery.

**How to change “New Video Gallery” name?**
In order to change Video Gallery name just double click on it’s name (on top tabs)

**I have already purchased Multi Site version, how do I upgrade it to Developer version, without buying it again?**
If you have any pro version our portfolio gallery plugin of our products and want to upgrade it, you do not need to buy the new portfolio gallery once again, you only need to pay the difference price. For that simply contact info@huge-it.com or our online chat and we will help you to pay the difference.

**I’ve bought the commercial license video gallery, installed the file but the Plugin video gallery seems to be still in free version, what to do?**
After installation of pro version, in General Options all your changes will be saved!
If you still see "free version" notification after installation of pro version video gallery.
* 1) try to clean your cache files if this will not help.
* 2) delete, and install the plugin again.

**I have multy sites video gallery, and the video gallery plugin works only on one, but not other sites, why?**
If you use multy site (have number of sites) when you install a plugin in main page, it will not work on other pages. 
In order it could work there too, you should. 
1) deactivate the plugin from main page.
2) go to the other pages and activate it there one by one.
3) after all will be activated, go back to your main page and activate it.

**How can I make videos auto play in lightbox?**
This feature is not provided in original plugin, but contacting info@huge-it.com you can order additional customization on this feature.

**I don't see thumbnail of video, why?**
If your video is Vimeo, make sure that it allows to share thumbnail image
Video shouldn't be private or unlisted
We also have adding "custom thumbnail image in gallery" 
With other support on this issue contact info@huge-it.com


**How to remove related videos in Video Gallery?**
This feature is not provided in original plugin but contacting info@huge-it.com you can order additional customization on this feature.


**I Put the video Link, and the thumbnail image is not showing**
Please check if your video is not private or unlisted, change the video link, if the problem happens with all videos, contact support info@huge-it.com


**How to turn of related videos in Video Gallery?**
It is recommended if for this you ask our developers info@huge-it.com We will prepare a new file for you without related videos, you just need to clarify what version you use and which view.


**May I add my own hosted videos in Video Gallery plugin?**
No, the Video Gallery is made to display only video links from Youtube Or Vimeo. 

**How do I disable related videos In Video Gallery?**
Our company can provide you a free customization service in video gallery. By contacting us info@huge-it.com and asking this, you may get a new plugin without related videos. In the request don't forget to mention: which version of video gallery you use, and on which view you need to disable rel videos.


**How can I sort the videos in video gallery plugin?**
If you want to change the order of the uploaded videos try the most common drag and drop function in the admin page of the Video Gallery plugin.


**Is there any limits  in uploaded video amount in video gallery?**
No, in both free and pro version of video gallery you can add as many video links as you need.


**I can not add my Video in the video gallery, why?**
The reason for that properly in the type of the video, in video gallery videos should be from Youtube and Vimeo and most important the video should be public, and not private or unlisted. 


If you think, that you found a bug in our [WordPress Video Gallery](http://huge-it.com/wordpress-video-gallery/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com).

###  Wordpress Video gallery
    
== Changelog ==

= 1.6.1 =
*  Bugs has been fixed on Video Gallery.

= 1.6.0 =
*  Mirror Bugs has been fixed on Video Gallery.

= 1.5.9 =
*  Bugs has been fixed on Video Gallery.

= 1.5.8 =
*  Bugs has been fixed on Video Gallery.

= 1.5.7 =
*  Bugs has been fixed on Video Gallery.

= 1.5.6 =
*  Mirror Bugs has been fixed on Video Gallery.

= 1.5.5 =
*  Conflict bug has been fixed on Video Gallery.

= 1.5.4 =
*  Some mirror Bugs has been fixed on Video Gallery.

= 1.5.3 =
* Backend Bugs has been fixed on Video Gallery.

= 1.5.2 =
*  Front end Bugs has been fixed on Video Gallery.

= 1.5.1 =
*  Admin bugs has been fixed on Video Gallery.

= 1.5.0 =
*  Bugs has been fixed on Video Gallery.

= 1.4.9 =
*  Mirror Bugs has been fixed on Video Gallery.

= 1.4.8 =
*  front end Bugs has been fixed on Video Gallery.

= 1.4.7 =
*  Some mirror Bugs has been fixed on Video Gallery.

= 1.4.6 =
*  front end Bugs has been fixed on Video Gallery.

= 1.4.5 =
*  Admin & front end vimeo Bugs has been fixed on Video Gallery.

= 1.4.4 =
*  Other plugins compatibility bug has been fixed in video gallery.

= 1.4.3 =
*  Bugs has been fixed on Video Gallery.

= 1.4.2 =
*  Bugs has been fixed on Video Gallery.

= 1.4.1 =
*  Some Mirror Bugs has been fixed on Video Gallery.

= 1.4.0 =
*  Bugs has been fixed on Video Gallery.

= 1.3.9 =
*  Bugs has been fixed on Video Gallery.

= 1.3.8 =
*  Bugs has been fixed on Video Gallery.

= 1.3.7 =
*  Bugs has been fixed on Video Gallery.

= 1.3.6 =
*  Bugs has been fixed on Video Gallery.

= 1.3.5 =
*  Bugs has been fixed on Video Gallery.

= 1.3.1 =
*  Added page navigation and load more on Video Gallery.

= 1.3.0 =
*  With this version you can add custom thumbnail for each video.

= 1.2.9 =
*  Added new view on Video Gallery.

= 1.2.8 =
*  Bugs has been fixed on Video Gallery.

= 1.2.7 =
*  Bugs and w3c has been fixed on Video Gallery.

= 1.2.6 =
*  Bugs has been fixed on Video Gallery.

= 1.2.5 =
*  Security bug fixed on Video Gallery.

= 1.2.4 =
*  Bugs has been fixed on Video Gallery.

= 1.2.3 =
*  Bugs have been fixed on Video Gallery.

= 1.2.2 =
*  Bugs have been fixed on Video Gallery.

= 1.2.1 =
*  Added new view Justified on Video Gallery.

= 1.2.0 =
*  Bugs have been fixed on Video Gallery.

= 1.1.9 =
*  Bugs have been fixed on Video Gallery.

= 1.1.8 =
*  Bugs have been fixed on Video Gallery.

= 1.1.7 =
*  Bugs have been fixed on Video Gallery.

= 1.1.6 =
*  Bugs have been fixed on Video Gallery.

= 1.1.5 =
*  Bugs have been fixed on Video Gallery.

= 1.1.4 =
*  Alt tag added in images on Video Gallery.

= 1.1.3 =
*  Bugs have been fixed on Video Gallery.

= 1.1.2 =
*  Bugs have been fixed on Video Gallery.

= 1.1.1 =
*  Bugs have been fixed on Video Gallery.

= 1.1.0 =
*  Bugs have been fixed on Video Gallery.

= 1.0.9 =
*  Two or more video galleries already available on one page.

= 1.0.8 =
*  Bugs have been fixed on Video Gallery.

= 1.0.7 =
*  Bugs have been fixed on Video Gallery's lightbox.

= 1.0.6 =
*  Youtube links bugs has been fixed in Video Gallery .

= 1.0.5 =
*  Video Gallery bug fixed on Video Gallery.

= 1.0.4 =
*  Notices errors has been fixed in Video Gallery.

= 1.0.3 =
*  SQL injection bug fixed in Video Gallery.

= 1.0.2 =
*  Video Gallery bug fixed on Video Gallery.

= 1.0.1 =
*  Video Gallery bug fixed on Video Gallery.

= 1.0.0 =
*  Video Gallery added.


==Wordpress Adding a Video Gallery==

### Step 1 Creating a Video Gallery

Huge-IT Video Gallery > Add New Video Gallery

* Add video link. Video links can be add from Youtube and Vimeo
* Hover/ Set Custom Thumbnail. On the video can be added another thumbnail image
* Title. Wright a title to the video
* Description. Wright some information about video content
* URL.You can add a link to another page that is related to the video
* View Counter. This Button show the number of video watching, hover on it to download the report in pdf file

### Inserting Created Video Gallery.

After you created Video Gallery on the right side find 3 blocks

* 1.Views. In this block you can choose one from 5 unrepeatable views
* 2.Usage. Here is located the shortcode Of your Video Gallery. By Copy&Paset up this shortcode can be inserted wherever you need in your post. Or it is possible to add the shortcode automatically. On post window just pressing on add video gallery button the shortcode will be inserted into post
* 3.Template include. Insert this code into your template,to make the inserting the video gallery easy for using in custom location within theme

### Step 2. General Options of Video Gallery

In this section you can modify your Video Gallery in more details. That will change the features of each view you choose.

### 2.1 Video Gallery/Content popup

Video element Styles

Video Video-element Width. Specify preferable width of your videos

Video element Height. Specify desired high of posted videos

Video element Border Width. Specify preferable width for surrounded border of the Video-element

Video element Border colour. Select preferable colour for surrounded border

Video element's Video-image Overlay colour. Select a colour for the overlay on the video as you hold the mouse arrow on it

Video element's Video-image Overlay Transparency. Determine preferable transparency degree for the video overlay

Zoom image Style. Determine black or white colour for zoom icon

**Popup Styles**

* Popup Background color. Edit to set preferable background colour of popup in your
* Popup Overlay color.Choose preferable colour for popup overlay in your
* Popup Overlay Transparency. Specify preferable degree of background transparency in your 
* Popup Close Button Style. Edit what  colour for  “X” icon would you like
* Show Separator Lines. Select to show separation lines between title and text in popup

**Popup Description**

* Show Description. Select to show the description of the video
* Description Font Size. Determine preferable size of description font
* Description Font color. Set preferable colour of description text

**Video-element Title**

* Video-element Title Font Size. Edit preferable size of title font in
* Video-element Title Font color. Edit preferable colour of title
* Video-element Title Background color. Determine preferable colour of title’s background

**Video-element Link button**

* Show Link button on Video-element. Select to show “View More” button on the video
* Link button Text. You can change the “View More” button text
* Link button Font Size. Choose preferable size of Link-button
* Link button Font color. Determine preferable colour of link font
* Link button Background color. Determine preferable colour for link background

**Popup Title**

* Popup Title Font Size. Determine title size of the letters of popup
* Popup Title Font color. Configure preferable colour for title in popup

**Popup Link button**

* Show Link button. Choose to make Link-button visible in popup
* Link button Text. Edit the text of Link-button in popup
* Link button Font Size. Set preferable size of the letters of Link-button in popup
* Link button Font color. Configure the preferable colour for Link-button in popup
* Link button Font Hover color. Determine preferable colour of Link-button when you hover the mouse on it
* Link button Background color. Specify preferable background colour of the Link-button in popup
* Link button Background Hover color. Specify preferable background colour of the link as you hover the mouse on it

### 2.2 Content video slider

**Slider Container**

* Main image Width. This fixes the margin of the main image
* Slider Background color. You can choose preferable colour for slider field
* Arrow Icons Style. Specify black/white colour of arrows in slider
* Show Separator Lines. Choose to make the lines between text, title, and link visible

**Title**

* Title Font Size. Configure the preferable size of the letters of the title
* Title Font color. Configure the preferable colour of the font

**Link button**

* Show Link button. Click to show the Link-button
* Link button Text. Write a text on link button
* Link button Font Size. Edit the size of the letters of the link text
* Link button Font color. Edit the colour of the link text
* Link button Font Hover color. Edit the colour of the link text while hovering the mouse on it
* Link button Background color. Determine preferable colour for link field
* Link button Background Hover color. Choose a colour for your link background while hovering on it

**Description**

* Show Description. Click to show the description of the text
* Description Font Size. Specify preferable size of the letters of description
* Description Font color. Choose a colour for description text in your “content video slider” view

### 2.3 LIghtbox-Video Gallery

**Video image**

* Video-image Width. Specify preferable size of the video
* Video-image Border Width. Specify preferable width of surrounded borders
* Video-image Border color. Configure your preferable the colour for border
* Border Radius. Determine prefered radius of border corners

**Title**

* Title Font Size. Specify the size for font of the title
* Title Font color. Configure the preferable colour for title
* Title Font Hover color. Configure the preferable of the title while hovering on it
* Title Background color. Change title background colour, which comes up in closed position
* Title Background Transparency. Set level for title background transparency

### 2.4 Video Slider

**Video Slider**

Video Behaviour. Choose “resized” to stretch and fit your videos to the size of Slider
Slider Background color. Set preferable color of the vacant part of slider field while images has its natural size
Slideshow Border Size. Specify the size of the border in your slider
Slideshow Border color. Choose your prefered colour for border
Slideshow Border radius Determine preferable radius for slider border

**Description**

* Description Width. Select the width of description text box
* Description Has Margin. Determine if description need to have margin
* Description Font Size. Specify the size of the letters in description
* Description Text color. Determine preferable color of the description text
* Description Text Align. Specify the location of the description in the box
* Description Background Transparency. Determine background transparency degree for description
* Description Background colour. Determine the colour for description field
* Description Border Size. Specify preferable size for the description border
* Description Border color. Specify preferable colour of description border
* Description Border Radius . Specify preferable corner’s radius of the description box
* Description Position. Specify where to posite the description on the slider view

**Title**

* Title Width. Determine the width for title box
* Title Has Margin. Click if you'd like the title to have margin
* Title Font Size. Edit to set preferable size of the letters for the title
* Title Text color. Edit to set preferable colour for the title
* Title Text Align. Specify the place of the title in the box
* Title Background Transparency
* Title Background color. Determine preferable colour of title background
* Title Border Size. Specify preferable size of the title border
* Title Border color. Determine preferable colour for the title border
* Title Border Radius. Specify preferable radius for border corners
* Title Position. Specify preferable position of the title

**Navigation**

* Show Navigation Arrows. Click to show navigation arrows in slider
* Navigation Dots Position / Hide Dots. Choose where to locate the dots, or choose to remove them
* Navigation Dots color. Choose the colour for navigation dots of slider
* Navigation Active Dot colour. Determine preferable colour of moving dots in slider
* Navigation Type.  Determine preferable type of your navigation arrows

### 2.5 Thumbnails

**Image**

* Video-image Behavior. Click to Determine behavior of video image
* Video-image Width. Allows to specify preferable width of thumbs
* Video-image Height. Allows to specify preferable height of thumbs
* Video-image Border Width. Allows to specify preferable width of the border between thumbs
* Video-image Border color.  Determine preferable colour of the border between thumbs
* Border Radius. Allows to specify preferable radius of the border
* Margin Video. Allows to specify the distance between each thumb


**Container Style**

* Presence of a background. Click to have background for thumbs
* Box background color. Specify desired colour of surrounded box
* Box shadow. Click if you'd like to have shadows in the box
* Box padding. Specify desired distance between box and video

**Title**

* Title Font Size. Specify preferable size of the text font
* Title Font color. determine preferable colour of text
* Overlay Background color. Determine the overlay colour of the title
* Title Background Transparency. Specify the degree of background transparency
* Link text. Wright desired text for the link

**2.6 Justified View**

**Element Styles**

* Image height. Set the height of the images/videos in tape
* Image margin. Set the distance between each image/video in the tape
* Image Justify. Select to make the width of all image/video up to 100% in container. So all them together will fit the container of your theme, and tick off to bring each of them with it’s natural size
* Image Randomize. Select to show your images randomly
* Opening With Animation. Choose to see the tape of images/videos appearing animated
* Opening Animation Speed. Select the speed of the animation

**Element Title**

* Show Title. Choose to show the title or not
* Element Title Font Size. Choose the font size for the title of the image/video
* Element Title Font Color. Select preferable colour for the title of the image/video
* Element Title Background Color. Choose the colour of title background
* Element’s Title Overlay Transparency. Select the level of transparency for the title overlay

**2.7 Block Styles Gallery**

**General Styles**

* Container Width. Choose whole gallery item container width
* Video Width. Choose video player  width
* Video Height. Choose video player height
* Video Position. Decide where to place the video
* Content Position. Set content position within page container
* Video Arrangement. Decide where to place the video among description and title
* Space Between Containers. Set padding level between each item container
* Separator Line Style. Pick up one style for separational line, which will divide your gallery items
* Separator Line Size. Anjust separational line size
* Separator Line Color. Choose a color for separational line
* Video Per Page. Decide how many videos to display in each page
* Pagination Font Size. Choose font size for pagination numbers
* Pagination Font Color. Change color for pagination font
* Pagination Icons Size. Decide the size of the icons on pagination
* Pagination Icons Color. Change the color of pagination icons
* Pagination Position. Decide where to place the navigation of pages

**Title Styles**

* Show Title. Select, if you need to display the title of the project
* Font Size. Choose title font size
* Font Color. Decide font color for the title
* Background Color. title has background, pick up a color for it
* Background Transparency. Adjust title background Transparency level
* Text Align. Choose text align for title

**Description Styles**

* Show Description. Select whether to see the gallery description text or not
* Font Size. Change the font size for the gallery description
* Font Color. Pick up font color for gallery description text
* Background Color. Change color for gallery description background
* Background Transparency. Adjust gallery description background transparency level
* Text Align. Chose the text align for gallery description

### Video Gallery General Options (PRO)

### Video Gallery / Content-popup (PRO)

**Video Gallery Content Styles**

**Show Content In The Center**

Do you want your videos to be in the centre of your page ? If you want then click in the square next to Show content in the center

###Video Gallery Element Styles

**Video Thumb behavior**

Here you can choose the size of your elements on your page. The natural is normal size for videos. Click on resize if you want your elements to be a bit larger.

**Video Gallery - Element Width And Height**

Write number next to element height and video-image width and examine the size of your video gallery elements. If the size is perfect then go on

**Element border width and color**

You can also have border surrounding your video-image and you can choose not only  it's width but also its color

**Element's Video-Image Overlay Color and Transparency**

For your video gallery ,select an overlay color, it can be any color you like. It will appear on your element very transparent when you want to zoom the video. You can also choose the percentage of your video-image overlay transparency 

**Video Gallery Zoom Image Style**

When zooming the video-image you can not only change the percentage of overlay

transparency ,but also you can turn it light or dark

###Video Gallery Popup Styles

**Video Gallery Popup Background Color**

When you click on video-image appears a popup with a text on it and a video. The popup has its color ,choose the color in this option by clicking in any of them and selecting the shade.

**Video Gallery Popup Overlay Color And Transparency**

Surrounding the popup you can see overlay, this overlay ca also have any color and any shade. Choose its color and its transparency percentage

**Video Gallery Popup Close Button Style**

Change the popup close button shade if you like. Select light or dark and  the close button color will be changed

**Video Gallery Show Separator Lines**

Click on the square next to this option when you want your text title to be separated from the thin text by a line

### Video Gallery Popup Link Button

**Show Link Button**

When you enter in the video popup you can see Link button under the text. If you don't want to see that button under the text then just click on the square next to this option

**Video Gallery Link  Button Text**

You can write the link button text by yourself . Change the view more to anything you like

**Video Gallery Link Button Font Size**

You can choose the link button font size , just write the size number  and see if that size is great for you. Link Button Font Color And Hover Color Choose the font color of the link button from all this colors, then the hover color. The hover color is used when you put the mouth on the link button

**Video Gallery Link Button Background Color And Hover Color**

Right after setting the font colors ,it's time for choosing the link button background color. And again select the color that you'd like to have as a background color. Under background color option you can see the background hover color option ,this color is used when your mouth is on button

### Video Gallery Pagination Styles

**Video Gallery Pagination Font Size And Color**

When you choose pagination style you can see the numbers under the video-images. You can change its font size by writing a number in this option. After setting the size ,it's time to set the color which you like for the font

**Video Gallery Pagination Icons Size and Color**

The icons  that you can see next to font write a number  for its  size. When the size is already set , it's time for its color .Choose the color that you like for icons

**Video Gallery Pagination Position**

Pagination position is for font and size. Choose what you want right , left or centre for its position under the videos

**Video Gallery Content Styles**

**Video Gallery Show Content In The Center**

Do you want your videos to be in the centre of your page ? If you want then click in the square next to Show content in the center

### Video Gallery Element Styles

**Video Gallery Video Thumb behavior**

Here you can choose the size of your elements on your page. The natural is normal size for videos. Click on resize if you want your elements to be a bit larger.

**Video Gallery Element Width And Height**

Write number next to element height and video-image width and examine the size of your video gallery elements. If the size is perfect then go on

**Video Gallery Element border width and color**

You can also have border surrounding your video-image and you can choose not only it's width but also its color

**Video Gallery Element's Video-Image Overlay Color and Transparency**

For your video gallery ,select an overlay color, it can be any color you like. It will appear on your element very transparent when you want to zoom the video. You can also choose the percentage of your video-image overlay  transparency

**Video Gallery Zoom Image Style**

When zooming the video-image you can not only change the percentage of overlay

transparency ,but also you can turn it light or dark.

### Video Gallery Popup Styles**

**Video Gallery Popup Background Color**

Video gallery - When you click on video-image appears a popup with a text on it and a video. The popup has its color ,choose the color in this option by clicking in any of them and selecting the shade.

**Video Gallery Popup Overlay Color And Transparency**

Video gallery - Surrounding the popup you can see overlay, this overlay ca also have any color and any shade. Choose its color and its transparency percentage.

**Video Gallery Popup Close Button Style**

Change the popup close button shade if you like. Select light or dark and the close button color will be changed.

**Video Gallery Show Separator Lines**

Video gallery - Click on the square next to this option when you want your text title to be separated from the thin text by a line.

### Video Gallery Popup Link Button

**Video Gallery Show Link Button**

When you enter in the video popup you can see Link button under the text of video gallery. If you don't want to see that button under the text then just click on the square next to this option

**Video Gallery Link  Button Text**

You can write the link button text by yourself. Change the view more to anything you like

**Video Gallery Link Button Font Size**

Video Gallery - You can choose the link button font size, just write the size number and see if that size is great for you. Link Button Font Color And Hover Color Choose the font color of the link button from all this colors, then the hover color. The hover color is used when you put the mouth on the link button.

**Video Gallery Link Button Background Color And Hover Color**

Video Gallery - Right after setting the font colors, it's time for choosing the link button background color video gallery. And again select the color that you'd like to have as a background color. Under background color option you can see the background hover color option ,this color is used when your mouth is on button.

### Pagination Styles

**Video Gallery Pagination Font Size And Color**

Video Gallery - When you choose pagination style you can see the numbers under the video-images. You can change its font size by writing a number in this option. After setting the size ,it's time to set the color which you like for the font

**Video Gallery Pagination Icons Size and Color**

Video Gallery - The icons video gallery that you can see next to font write a number  for its  size . When the size is already set in video gallery, it's time for its color. Choose the color that you like for icons

**Video Gallery Pagination Position**

Video Gallery - Pagination position gallery is for font and size. Choose what you want right , left or centre for its position under the videos.

If you think, that you found a bug in our [WordPress Video Gallery](http://huge-it.com/wordpress-video-gallery/) plugin or have any question contact us at [info@huge-it.com](mailto:info@huge-it.com)